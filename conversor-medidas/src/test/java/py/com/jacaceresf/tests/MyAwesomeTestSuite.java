package py.com.jacaceresf.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by jcaceresf on 11/1/20
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({Tabla1Test.class, Tabla2Test.class})
public class MyAwesomeTestSuite {
}
