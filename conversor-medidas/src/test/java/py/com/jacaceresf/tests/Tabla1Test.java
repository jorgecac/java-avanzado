package py.com.jacaceresf.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import py.com.jacaceresf.conversor.Tabla1;

/**
 * Created by jcaceresf on 11/1/20
 */
public class Tabla1Test {

    public Tabla1 conversor1;

    @Before
    public void init() {
        conversor1 = new Tabla1();
    }

    @Test
    public void decimalToBinaryTest() {
        int decimalValue = 1997;
        String binaryValue = conversor1.decimalToBinary(decimalValue);
        System.out.printf("Binary value of [%d] is [%s] \n", decimalValue, binaryValue);
        Assert.assertEquals("11111001101", binaryValue);
    }

    @Test(expected = AssertionError.class)
    public void decimalToBinaryInvalidTest() {
        int decimalValue = 1997;
        String binaryValue = conversor1.decimalToBinary(decimalValue);
        System.out.printf("Error: Binary value of [%d] is [%s] \n", decimalValue, binaryValue);
        Assert.assertEquals("1111100101", binaryValue);
    }


    @Test
    public void decimalToHexTest() {
        int decimalValue = 252;
        String hexValue = conversor1.decimalToHex(decimalValue);
        System.out.printf("Hex value of [%d] is [%s] \n", decimalValue, hexValue);
        Assert.assertTrue("FC".equalsIgnoreCase(hexValue));
    }

    @Test
    public void binaryToHexTest() {
        String binaryValue = "100011110011";
        String hexValue = conversor1.binaryToHex(binaryValue);
        System.out.printf("Hex value of [%s] is [%s]\n\n", binaryValue, hexValue);
        Assert.assertTrue("8F3".equalsIgnoreCase(hexValue));
    }

    @Test
    public void binaryToDecimal() {
        String binaryValue = "10101001111011";
        int decimalValue = conversor1.binaryToDecimal(binaryValue);
        System.out.printf("Decimal value of [%s] is [%d]\n", binaryValue, decimalValue);
        Assert.assertEquals(10875, decimalValue);
    }

    @Test
    public void hexToBinaryTest() {
        String hexValue = "9AF";
        String binaryValue = conversor1.hexToBinary(hexValue);
        System.out.printf("Binary value of [%s] is [%s]\n", hexValue, binaryValue);
        Assert.assertEquals("100110101111", binaryValue);
    }

    @Test
    public void hexToDecimalTest() {
        String hexValue = "45AF";
        int decimalValue = conversor1.hexToDecimal(hexValue);
        System.out.printf("Decimal value of [%s] is [%d]\n", hexValue, decimalValue);
        Assert.assertEquals(17839, decimalValue);
    }

}
