package py.com.jacaceresf.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import py.com.jacaceresf.conversor.Tabla2;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * Created by jcaceresf on 11/1/20
 */
public class Tabla2Test {

    private Tabla2 conversor2;

    private BigDecimal meterValue;

    private BigDecimal biggerMeterValue;

    @Before
    public void init() {
        conversor2 = new Tabla2();
        meterValue = new BigDecimal("150");
        biggerMeterValue = new BigDecimal("35000");
    }


    @Test
    public void meterToMmTest() {
        BigDecimal millimeter = conversor2.meterToMm(meterValue);
        System.out.printf("[%s] meters is equal to [%s] millimeters\n", meterValue, millimeter);
        assertEquals(0, millimeter.compareTo(new BigDecimal("150000")));
    }

    @Test(expected = AssertionError.class)
    public void meterToMmInvalidTest() {
        BigDecimal millimeter = conversor2.meterToMm(meterValue);
        System.out.printf("Error: [%s] meters is equal to [%s] millimeters\n", meterValue, millimeter);
        assertEquals(0, millimeter.compareTo(new BigDecimal("189000")));
    }

    @Test
    public void meterToCmTest() {
        BigDecimal centimeter = conversor2.meterToCm(meterValue);
        System.out.printf("[%s] meters is equal to [%s] centimeters\n", meterValue, centimeter);
        assertEquals(0, centimeter.compareTo(new BigDecimal("15000")));
    }

    @Test
    public void meterToDmTest() {
        BigDecimal decimeter = conversor2.meterToDm(meterValue);
        System.out.printf("[%s] meters is equal to [%s] decimeters\n", meterValue, decimeter);
        assertEquals(0, decimeter.compareTo(new BigDecimal("1500")));
    }

    @Test
    public void meterToDamTest() {
        BigDecimal decameter = conversor2.meterToDam(meterValue);
        System.out.printf("[%s] meters is equal to [%s] decameters\n", meterValue, decameter);
        assertEquals(0, decameter.compareTo(new BigDecimal("15")));
    }

    @Test
    public void meterToHmTest() {
        BigDecimal hectometer = conversor2.meterToHm(meterValue);
        System.out.printf("[%s] meters is equal to [%s] hectometers\n", meterValue, hectometer);
        assertEquals(0, hectometer.compareTo(new BigDecimal("1.5")));
    }

    @Test
    public void meterToKmTest() {
        BigDecimal kilometer = conversor2.meterToKm(biggerMeterValue);
        System.out.printf("[%s] meters is equal to [%s] kilometers\n", biggerMeterValue, kilometer);
        assertEquals(0, kilometer.compareTo(new BigDecimal("35.0")));
    }

    @Test
    public void meterToInchTest() {
        BigDecimal inches = conversor2.meterToInch(meterValue);
        System.out.printf("[%s] meters is equal to [%s] inches\n", meterValue, inches);
        assertEquals(0, inches.compareTo(new BigDecimal("5905.50")));
    }

    @Test
    public void meterToYardTest() {
        BigDecimal yards = conversor2.meterToYard(meterValue);
        System.out.printf("[%s] meters is equal to [%s] yards\n", meterValue, yards);
        assertEquals(0, yards.compareTo(new BigDecimal("164.100")));
    }

    @Test
    public void meterToMile() {
        BigDecimal miles = conversor2.meterToMile(biggerMeterValue);
        System.out.printf("[%s] meters is equal to [%s] miles\n", biggerMeterValue, miles);
        assertEquals(0, miles.compareTo(new BigDecimal("22")));
    }

    @Test
    public void meterToAngstromTest() {
        BigDecimal angstroms = conversor2.meterToAngstrom(meterValue);
        System.out.printf("[%s] meters is equal to [%s] angstroms\n", meterValue, angstroms);
        assertEquals(0, angstroms.compareTo(new BigDecimal("1500000000000")));
    }

    @Test
    public void meterToMicronTest() {
        BigDecimal microns = conversor2.meterToMicron(meterValue);
        System.out.printf("[%s] meters is equal to [%s] microns\n", meterValue, microns);
        assertEquals(0, microns.compareTo(new BigDecimal("150000000")));
    }

    @Test
    public void meterToFeet() {
        BigDecimal feet = conversor2.meterToFeet(meterValue);
        System.out.printf("[%s] meters is equal to [%s] feet\n", meterValue, feet);
        assertEquals(0, feet.compareTo(new BigDecimal("492.150")));
    }
}
