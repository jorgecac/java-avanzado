package py.com.jacaceresf.conversor;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created by jcaceresf on 11/1/20
 */
public class Tabla2 {

    /**
     * metro -> milimetro
     * metro -> centimetro
     * metro -> decimetro
     * metro -> decametro
     * metro -> hectometro
     * metro -> kilometro
     * metro -> pulgada
     * metro -> pia
     * metro -> yarda
     * metro -> milla
     * metro -> angstrom
     * metro -> micron
     */

    public BigDecimal meterToMm(BigDecimal value) {
        return value.multiply(new BigDecimal("1000"));
    }

    public BigDecimal meterToCm(BigDecimal value) {
        return value.multiply(new BigDecimal("100"));
    }

    public BigDecimal meterToDm(BigDecimal value) {
        return value.multiply(new BigDecimal("10"));
    }

    public BigDecimal meterToDam(BigDecimal value) {
        return value.divide(new BigDecimal("10"), 2);
    }

    public BigDecimal meterToHm(BigDecimal value) {
        return value.divide(new BigDecimal("100"), 1, RoundingMode.CEILING);
    }

    public BigDecimal meterToKm(BigDecimal value) {
        return value.divide(new BigDecimal("1000"), 1, RoundingMode.CEILING);
    }

    public BigDecimal meterToInch(BigDecimal value) {
        return value.multiply(new BigDecimal("39.37"));
    }

    public BigDecimal meterToYard(BigDecimal value) {
        return value.multiply(new BigDecimal("1.094"));
    }

    public BigDecimal meterToMile(BigDecimal value) {
        return value.divide(new BigDecimal("1609"), 0, RoundingMode.CEILING);
    }

    public BigDecimal meterToAngstrom(BigDecimal value) {
        return value.multiply(new BigDecimal("10000000000"));
    }

    public BigDecimal meterToMicron(BigDecimal value) {
        return value.multiply(new BigDecimal("1000000"));
    }

    public BigDecimal meterToFeet(BigDecimal value) {
        return value.multiply(new BigDecimal("3.281"));
    }
}
