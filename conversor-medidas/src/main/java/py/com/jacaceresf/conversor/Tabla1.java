package py.com.jacaceresf.conversor;

/**
 * Created by jcaceresf on 11/1/20
 */
public class Tabla1 {

    /**
     * decimal -> binario
     * decimal -> hexadecimal
     * binario -> hexadecimal
     * binario -> decimal
     * hexadecimal -> binario
     * hexadecimal -> decimal
     */

    public String decimalToBinary(int decimal) {
        return Integer.toBinaryString(decimal);
    }


    public String decimalToHex(int decimal) {
        return Integer.toHexString(decimal);
    }

    public String binaryToHex(String binary) {
        //1 - Take the binary and convert it to an integer representation.
        //2 - Take the result and convert it to hex representation.
        return Integer.toString(Integer.parseInt(binary, 2), 16);
    }

    public int binaryToDecimal(String binary) {
        return Integer.parseInt(Integer.toString(Integer.parseInt(binary, 2), 10));
    }

    public String hexToBinary(String hex) {
        return Integer.toString(Integer.parseInt(hex, 16), 2);
    }

    public int hexToDecimal(String hex) {
        return Integer.parseInt(Integer.toString(Integer.parseInt(hex, 16), 10));
    }
}
