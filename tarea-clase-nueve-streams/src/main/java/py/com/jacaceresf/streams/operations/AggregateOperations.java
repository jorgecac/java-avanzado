package py.com.jacaceresf.streams.operations;

import py.com.jacaceresf.streams.beans.Customer;
import py.com.jacaceresf.streams.utils.CustomerGenerator;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jcaceresf on 10/9/20
 */
public class AggregateOperations {

    public void aggregateByBank() {
        System.out.println("Suma de saldos por Banco.");
        List<Customer> customers = CustomerGenerator.getCustomerList();
        customers.stream()
                .collect(Collectors.groupingBy(Customer::getBankName, Collectors.summingInt(Customer::getBalance)))
                .forEach((bank, sum) -> System.out.println(bank + ": " + sum));

    }

    public void averageByBank() {
        System.out.println("Promedio de saldo por Banco.");
        List<Customer> customers = CustomerGenerator.getCustomerList();
        customers.stream()
                .collect(Collectors.groupingBy(Customer::getBankName, Collectors.toList()))
                .forEach((key, value) -> System.out.println(key + ":" + value.stream().collect(Collectors.averagingInt(Customer::getBalance))));
    }

    public void maxBalanceByBank() {
        List<Customer> customers = CustomerGenerator.getCustomerList();
        System.out.println("Mayor saldo por Banco.");
        customers.stream()
                .collect(Collectors.groupingBy(Customer::getBankName, Collectors.toList()))
                .forEach((key, value) -> System.out.println("Cliente " + value.stream().max(Comparator.comparing(Customer::getBalance)).get() + " tiene mas saldo que nadie en el " + key));
    }

}
