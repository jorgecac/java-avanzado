package py.com.jacaceresf.streams.operations;

import py.com.jacaceresf.streams.beans.Customer;
import py.com.jacaceresf.streams.utils.CustomerGenerator;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by jcaceresf on 10/9/20
 * <p>
 * Operación de agrupación(collect) y filtro de tipo having (mínimo dos ejemplos).
 * <p>
 * sources =>   https://stackoverflow.com/questions/36261036/implementation-of-having-count-with-group-by-in-java-8
 * https://mkyong.com/java8/java-8-collectors-groupingby-and-mapping-example/
 */
public class CollectorOperations {

    public void collect() {
        List<Customer> listToCollect = CustomerGenerator.getCustomerList();
        System.out.println("Clientes por banco");
        listToCollect.stream()
                .collect(Collectors.groupingBy(Customer::getBankName, Collectors.toList()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(entry -> System.out.printf("%s => %s \n", entry.getKey(), entry.getValue()));
    }

    public void havingByBalance() {

        List<Customer> customers = CustomerGenerator.getCustomerList();

        List<Map.Entry<String, Integer>> collect = customers.stream()
                .collect(Collectors.groupingBy(Customer::getBankName, Collectors.summingInt(Customer::getBalance)))
                .entrySet()
                .stream()
                .filter(customerBalance -> customerBalance.getValue() > 10000000)
                .collect(Collectors.toList());
        System.out.println("Agrupados por bancos y con suma de saldos mayor a 10000000");
        System.out.println(collect);
    }

}
