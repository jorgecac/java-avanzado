package py.com.jacaceresf.streams.operations;

/**
 * Created by jcaceresf on 10/9/20
 */
public class OperationsExecutor {

    public static void main(String[] args) {

        SortedOperations sortedOperations = new SortedOperations();
        CollectorOperations collectorOperations = new CollectorOperations();
        AggregateOperations aggregateOperations = new AggregateOperations();

        sortedOperations.sortByBalanceAndBornDate();
        System.out.print("\n------------------------------------------------------------\n\n");
        sortedOperations.sortByNameAndId();
        System.out.print("\n------------------------------------------------------------\n\n");

        collectorOperations.collect();
        System.out.print("\n\n------------------------------------------------------------\n\n");
        collectorOperations.havingByBalance();
        System.out.print("\n------------------------------------------------------------\n\n");


        aggregateOperations.aggregateByBank();
        System.out.print("\n------------------------------------------------------------\n\n");
        aggregateOperations.averageByBank();
        System.out.print("\n------------------------------------------------------------\n\n");
        aggregateOperations.maxBalanceByBank();
    }

}
