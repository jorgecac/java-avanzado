package py.com.jacaceresf.streams.beans;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

/**
 * Created by jcaceresf on 10/9/20
 */
public class Customer {

    private int id;
    private String name;
    private LocalDate bornDate;
    private Integer balance;
    private String email;
    private String bankName;

    public Customer(int id, String name, LocalDate bornDate, Integer balance, String email, String bankName) {
        this.id = id;
        this.name = name;
        this.bornDate = bornDate;
        this.balance = balance;
        this.email = email;
        this.bankName = bankName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBornDate() {
        return bornDate;
    }

    public void setBornDate(LocalDate bornDate) {
        this.bornDate = bornDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return String.format("%d | %s nacido en %s con saldo %d y email %s", id, name, bornDate, balance, email);
    }
}
