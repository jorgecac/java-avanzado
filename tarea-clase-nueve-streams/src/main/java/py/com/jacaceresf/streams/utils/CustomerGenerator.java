package py.com.jacaceresf.streams.utils;

import py.com.jacaceresf.streams.beans.Customer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by jcaceresf on 10/9/20
 */
public class CustomerGenerator {

    private static Random _random = new Random();

    public static List<Customer> getCustomerList() {

        List<Customer> customers = new ArrayList<>();

        customers.add(new Customer(_random.nextInt(1000000), "BRUCE WAYNE", LocalDate.of(1990, 10, 1), 1000000, "bruce@wayne.com", "Banco 1"));
        customers.add(new Customer(_random.nextInt(1000000), "TONY STARK", LocalDate.of(1992, 12, 2), 3000000, "tony@stark.com", "Banco 1"));
        customers.add(new Customer(_random.nextInt(1000000), "STEVEN RODGERS", LocalDate.of(1979, 9, 21), 4000000, "captain@america.com", "Banco 3"));
        customers.add(new Customer(_random.nextInt(1000000), "LIONEL MESSI", LocalDate.of(1992, 10, 14), 9800000, "lionel@messi.com", "Banco 2"));
        customers.add(new Customer(_random.nextInt(1000000), "RAFAEL NADAL", LocalDate.of(1980, 3, 15), 6700000, "rafa@nadal.com", "Banco 4"));
        customers.add(new Customer(_random.nextInt(1000000), "ROGER FEDERER", LocalDate.of(1977, 4, 12), 900000, "roger@federer.com", "Banco 2"));
        customers.add(new Customer(_random.nextInt(1000000), "MICHAEL JORDAN", LocalDate.of(1965, 5, 20), 3500000, "air@jordan.com", "Banco 4"));
        customers.add(new Customer(_random.nextInt(1000000), "LEBRON JAMES", LocalDate.of(2000, 2, 11), 5600000, "the@king.com", "Banco 2"));

        return customers;
    }

}
