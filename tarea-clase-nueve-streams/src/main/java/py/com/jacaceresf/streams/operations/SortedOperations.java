package py.com.jacaceresf.streams.operations;

import py.com.jacaceresf.streams.beans.Customer;
import py.com.jacaceresf.streams.utils.CustomerGenerator;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jcaceresf on 10/9/20
 * <p>
 * Source => https://mkyong.com/java8/java-8-how-to-sort-list-with-stream-sorted/
 */
public class SortedOperations {


    public void sortByNameAndId() {

        List<Customer> list = CustomerGenerator.getCustomerList();

        List<Customer> sortedList = list.stream()
                .sorted(Comparator.comparing(Customer::getName))
                .sorted(Comparator.comparingInt(Customer::getId))
                .collect(Collectors.toList());

        System.out.println("Clientes ordenados por nombre e ID");
        sortedList.forEach(System.out::println);
    }

    public void sortByBalanceAndBornDate() {

        List<Customer> list = CustomerGenerator.getCustomerList();

        Comparator<Customer> compareByBalanceAndBornDate = Comparator.comparing(Customer::getBalance).thenComparing(Customer::getBornDate);

        List<Customer> sortedList = list.stream()
                .sorted(compareByBalanceAndBornDate)
                .collect(Collectors.toList());

        System.out.println("Clientes ordenados por saldo y fecha de nacimiento.");
        sortedList.forEach(System.out::println);
    }

}
