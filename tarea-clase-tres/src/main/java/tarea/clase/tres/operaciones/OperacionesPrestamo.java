package tarea.clase.tres.operaciones;

import tarea.clase.tres.beans.Cliente;
import tarea.clase.tres.beans.Prestamo;
import tarea.clase.tres.utils.PrestamoUtil;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by jcaceresf on 8/20/20
 */
public class OperacionesPrestamo {

    private Set<Cliente> clientes;
    private Set<Prestamo> prestamos;

    public Set<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(Set<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Set<Prestamo> getPrestamos() {
        return prestamos;
    }

    public void setPrestamos(Set<Prestamo> prestamos) {
        this.prestamos = prestamos;
    }

    public OperacionesPrestamo() {
        this.clientes = new HashSet<>();
        this.prestamos = new HashSet<>();
    }


    public void agregarPrestamo() throws Exception {

        Scanner scanner = new Scanner(System.in);

        Prestamo prestamo = new Prestamo();

        System.out.println("Ingrese el numero de prestamo: ");
        prestamo.setNumeroPrestamo(scanner.nextInt());

        if (prestamos.stream().anyMatch(p -> p.getNumeroPrestamo() == prestamo.getNumeroPrestamo())) {
            System.out.println("No puede cargar un prestamo dos veces.");
            throw new Exception("Prestamo " + prestamo.getNumeroPrestamo() + " ya existe.");
        }

        prestamo.setCliente(
                obtenerCliente()
        );

        System.out.println("Ingrese el tipo de prestamo: ");
        prestamo.setTipoPrestamo(PrestamoUtil.obtenerTipoPrestamo());

        System.out.println("Ingrese la moneda del prestamo (Gs/USD) ");
        prestamo.setMoneda(PrestamoUtil.obtenerMoneda());

        System.out.println("Por favor, ingrese el plazo (en meses) del prestamo: ");
        prestamo.setPlazo(scanner.nextInt());
        prestamo.setCantidadCuotas(prestamo.getPlazo());

        System.out.println("Ingrese la tasa anual: ");
        prestamo.setTasaAnual(Double.parseDouble(scanner.next()));

        System.out.println("Ingrese el monto capital del prestamo: ");
        prestamo.setMontoCapital(scanner.nextBigDecimal());

        System.out.println("Ingrese el monto del interes: ");
        prestamo.setMontoInteres(scanner.nextBigDecimal());

        //datos predeterminados
        prestamo.setCantidadCuotasPagadas(0);
        prestamo.setSaldoCapital(BigDecimal.ZERO);
        prestamo.setSaldoInteres(BigDecimal.ZERO);


        prestamos.add(prestamo);

    }

    public void imprimirListaPrestamos() {
        prestamos.forEach(System.out::println);
    }

    public void actualizarSaldos() throws Exception {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el numero de prestamo: ");
        int numeroPrestamo = scanner.nextInt();

        System.out.println("Vamos a actualizar el prestamo " + numeroPrestamo);
        if (prestamos.stream().anyMatch(prestamo -> prestamo.getNumeroPrestamo() == numeroPrestamo)) {
            System.out.println("No se encuentra el prestamo " + numeroPrestamo);
            throw new Exception("No se encuentra el prestamo especificado");
        } else {

            System.out.println("Ingrese el interes del prestamo: ");
            BigDecimal interesActualizado = scanner.nextBigDecimal();

            System.out.println("Ingrese el capital actualizado: ");
            BigDecimal capitalActualizado = scanner.nextBigDecimal();

            System.out.println("Ingrese el numero de cuotas actualizada: ");
            int cuotasActualizadas = scanner.nextInt();

            for (Prestamo prestamo : prestamos) {
                if (prestamo.getNumeroPrestamo() == numeroPrestamo) {
                    validarPrestamo(prestamo);
                    prestamo.setSaldoInteres(interesActualizado);
                    prestamo.setSaldoCapital(capitalActualizado);
                    prestamo.setCantidadCuotas(cuotasActualizadas);
                    System.out.println("Prestamo actualizado -> " + prestamo);
                    break;
                }
            }
        }
    }

    /**
     * El saldo del capital debe ser menor o igual al monto del capital del préstamo
     * <p>
     * El saldo del interés debe ser menor o igual al monto del interés del préstamo.
     * <p>
     * La cantidad de cuotas pagadas debe ser menor o igual a la cantidad total de cuotas del préstamo.
     * <p>
     * El saldo del capital, el saldo del interés y la cantidad de cuotas pagadas no pueden ser negativos.
     *
     * @param prestamo
     * @throws Exception
     */
    private void validarPrestamo(Prestamo prestamo) throws Exception {
        if (prestamo.getSaldoCapital().compareTo(prestamo.getMontoCapital()) > 0) {
            throw new Exception("Saldo capital no es menor o igual al monto de capital.");
        }

        if (prestamo.getSaldoInteres().compareTo(prestamo.getMontoInteres()) > 0) {
            throw new Exception("Saldo interes no es menor o igual al monto de capital.");
        }

        if (prestamo.getCantidadCuotasPagadas() > prestamo.getCantidadCuotas()) {
            throw new Exception("Las cuotas pagadas no pueden ser mayor a la cantidad total de cuotas.");
        }

        if (prestamo.getCantidadCuotasPagadas() < 0) {
            throw new Exception("La cantidad de cuotas pagadas no puede ser negativa.");
        }

        if (prestamo.getSaldoCapital().compareTo(BigDecimal.ZERO) < 0) {
            throw new Exception("El saldo capital no puede ser negativo");
        }

        if (prestamo.getSaldoInteres().compareTo(BigDecimal.ZERO) < 0) {
            throw new Exception("El saldo del interes no puede ser negativo");
        }

    }

    /**
     * Eliminar el o los préstamos de un cliente de acuerdo con el número de documento ingresado por el usuario.
     */
    public void eliminarPrestamosCliente() {

        System.out.println("Ingrese el nro. de documento del cliente para eliminar sus prestamos: ");
        Scanner scanner = new Scanner(System.in);
        String numeroDocumento = scanner.next();
        System.out.println("Vamos a eliminar los prestamos del cliente: " + numeroDocumento);

        if (prestamos.stream().anyMatch(prestamo -> prestamo.getCliente().getNumeroDocumento().equals(numeroDocumento))) {

            prestamos.removeIf(prestamo -> prestamo.getCliente().getNumeroDocumento().equals(numeroDocumento));

        }

    }

    /**
     * Imprimir los datos de los préstamos, así como también los datos de los clientes. Los datos deben estar
     * ordenados por el tipo de préstamo, de la a-z.
     */
    public void imprimirDatosOrdenados() {

        List<Prestamo> prestamoList = prestamos.stream()
                .sorted(Comparator.comparing(Prestamo::getTipoPrestamo))
                .collect(Collectors.toList());
        prestamoList.forEach(System.out::println);
    }

    /**
     * Imprimir los datos de los préstamos, así como también los datos de los clientes.
     * Los datos deben estar ordenados por el saldo capital, descendentemente.
     */
    public void imprimirPrestamosOrdenadosPorSaldo() {
        List<Prestamo> prestamoList = prestamos.stream()
                .sorted(Comparator.comparing(Prestamo::getSaldoCapital))
                .collect(Collectors.toList());
        prestamoList.forEach(System.out::println);
    }

    public Cliente obtenerCliente() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese el numero de documento del cliente: ");
        String numeroDocumento = sc.next();

        return clientes.stream()
                .filter(cliente -> cliente.getNumeroDocumento().equals(numeroDocumento))
                .findFirst()
                .orElse(null);
    }

    public void agregarCliente() throws Exception {

        System.out.println("Por favor, ingrese los datos del cliente");

        Scanner scanner = new Scanner(System.in);
        Cliente cliente = new Cliente();

        System.out.println("Ingrese el numero de documento del cliente: ");
        cliente.setNumeroDocumento(scanner.next());

        if (clientes.stream().anyMatch(c -> c.getNumeroDocumento().equals(cliente.getNumeroDocumento()))) {
            System.out.println("No puede cargar un cliente dos veces.");
            throw new Exception("Cliente " + cliente.getNumeroDocumento() + " ya existe.");
        }

        System.out.println("Ingrese el nombre del cliente: ");
        cliente.setNombre(scanner.next());

        System.out.println("Ingrese el apellido del cliente: ");
        cliente.setApellido(scanner.next());

        System.out.println("Ingrese la edad del cliente: ");
        cliente.setEdad(scanner.nextInt());

        System.out.println("Ingrese el estado civil del cliente: ");
        cliente.setEstadoCivil(PrestamoUtil.obtenerEstadoCivil());

        System.out.println("Ingrese el telefono del cliente: ");
        cliente.setTelefono(scanner.next());

        System.out.println("Ingrese el email del cliente: ");
        cliente.setEmail(scanner.next());

        this.clientes.add(cliente);

    }
}
