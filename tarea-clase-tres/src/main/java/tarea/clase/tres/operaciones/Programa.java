package tarea.clase.tres.operaciones;

import sun.misc.Cleaner;
import tarea.clase.tres.beans.Cliente;
import tarea.clase.tres.beans.Prestamo;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLOutput;
import java.util.Scanner;

/**
 * Created by jcaceresf on 8/21/20
 */
public class Programa {

    static final String operatingSystem = System.getProperty("os.name");

    /**
     * Despliega el menu con todas las operaciones disponibles y obtiene la decision del usuario.
     *
     * @return
     */
    public static int obtenerDecisionUsuario() {

        int decision = 0;

        while (decision > 8 || decision < 1) {
            System.out.println("Decision => " + decision);
            Scanner scanner = new Scanner(System.in);
            System.out.println("1 - Agregar cliente");
            System.out.println("2 - Agregar prestamo");
            System.out.println("3 - Imprimir prestamos y cliente del prestamo");
            System.out.println("4 - Actualizar el saldo del capital, el saldo del interés y la cantidad de cuotas pagadas");
            System.out.println("5 - Eliminar prestamos de un cliente");
            System.out.println("6 - Imprimir prestamos ordenados por tipo de prestamo");
            System.out.println("7 - Imprimir prestamos ordenados por saldo capital");
            System.out.println("8 - Salir");
            decision = scanner.nextInt();
        }
        return decision;
    }

    /**
     * En base al numero ingresado por el usuario, ejecutamos una de las operaciones.
     *
     * @param decision
     * @param operacionesPrestamo
     * @throws Exception
     */
    public static void evaluarDecision(int decision, OperacionesPrestamo operacionesPrestamo) throws Exception {
        switch (decision) {
            case 1:
                operacionesPrestamo.agregarCliente();
                break;
            case 2:
                operacionesPrestamo.agregarPrestamo();
                break;
            case 3:
                operacionesPrestamo.imprimirListaPrestamos();
                break;
            case 4:
                operacionesPrestamo.actualizarSaldos();
                break;
            case 5:
                operacionesPrestamo.eliminarPrestamosCliente();
                break;
            case 6:
                operacionesPrestamo.imprimirDatosOrdenados();
                break;
            case 7:
                operacionesPrestamo.imprimirPrestamosOrdenadosPorSaldo();
                break;
            case 8:
                System.out.println("Finalizando ejecucion del programa.");
                break;
            default:
                System.out.println("Decision desconocida.");
                break;
        }
    }

    public static void limpiar() throws IOException {
        if (operatingSystem.contains("Windows")) {
            Runtime.getRuntime().exec("cls");
        } else {
            Runtime.getRuntime().exec("clear");
        }
    }

    public static void main(String[] args) {

        OperacionesPrestamo operacionesPrestamo = new OperacionesPrestamo();

        int decision = 0;

        while (decision != 8) {
            try {

                decision = obtenerDecisionUsuario();

                evaluarDecision(decision, operacionesPrestamo);

                limpiar();

            } catch (Exception e) {
                System.out.println("Ha ocurrido un error: " + e.getMessage());
                e.printStackTrace();
            }
        }


    }

}
