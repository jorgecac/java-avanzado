package tarea.clase.tres.beans;

import java.math.BigDecimal;

/**
 * Created by jcaceresf on 8/20/20
 */
public class Prestamo {

    private int numeroPrestamo;
    private String tipoPrestamo;
    private int plazo;
    private double tasaAnual;
    private String moneda;
    private BigDecimal montoCapital;
    private BigDecimal montoInteres;
    private BigDecimal saldoCapital;
    private BigDecimal saldoInteres;
    private int cantidadCuotas;
    private int cantidadCuotasPagadas;
    private Cliente cliente;

    public String getTipoPrestamo() {
        return tipoPrestamo;
    }

    public void setTipoPrestamo(String tipoPrestamo) {
        this.tipoPrestamo = tipoPrestamo;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public double getTasaAnual() {
        return tasaAnual;
    }

    public void setTasaAnual(double tasaAnual) {
        this.tasaAnual = tasaAnual;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getMontoCapital() {
        return montoCapital;
    }

    public void setMontoCapital(BigDecimal montoCapital) {
        this.montoCapital = montoCapital;
    }

    public BigDecimal getMontoInteres() {
        return montoInteres;
    }

    public void setMontoInteres(BigDecimal montoInteres) {
        this.montoInteres = montoInteres;
    }

    public BigDecimal getSaldoCapital() {
        return saldoCapital;
    }

    public void setSaldoCapital(BigDecimal saldoCapital) {
        this.saldoCapital = saldoCapital;
    }

    public BigDecimal getSaldoInteres() {
        return saldoInteres;
    }

    public void setSaldoInteres(BigDecimal saldoInteres) {
        this.saldoInteres = saldoInteres;
    }

    public int getCantidadCuotas() {
        return cantidadCuotas;
    }

    public void setCantidadCuotas(int cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    public int getCantidadCuotasPagadas() {
        return cantidadCuotasPagadas;
    }

    public void setCantidadCuotasPagadas(int cantidadCuotasPagadas) {
        this.cantidadCuotasPagadas = cantidadCuotasPagadas;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getNumeroPrestamo() {
        return numeroPrestamo;
    }

    public void setNumeroPrestamo(int numeroPrestamo) {
        this.numeroPrestamo = numeroPrestamo;
    }

    @Override
    public String toString() {
        return "Nro. Prestamo=" + numeroPrestamo +
                ", Tipo='" + tipoPrestamo + '\'' +
                ", Plazo=" + plazo +
                ", Tasa=" + tasaAnual +
                ", Moneda='" + moneda + '\'' +
                ", Capital=" + montoCapital +
                ", Interes=" + montoInteres +
                ", Saldo Capital=" + saldoCapital +
                ", Saldo Interes=" + saldoInteres +
                ", Cuotas=" + cantidadCuotas +
                ", Cuotas pagadas=" + cantidadCuotasPagadas +
                ", Cliente =" + cliente;
    }
}
