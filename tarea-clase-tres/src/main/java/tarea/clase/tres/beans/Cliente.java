package tarea.clase.tres.beans;

public class Cliente {

    //De cada cliente se necesita almacenar lo siguientes datos: número de documento,
    // nombre, apellido, edad, estado civil (S=soltero/a, C=casado/a, V=viudo/a, D=divorciado/a), teléfono y email.

    private String numeroDocumento;
    private String nombre;
    private String apellido;
    private int edad;
    private String estadoCivil;
    private String telefono;
    private String email;

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Nro. documento='" + numeroDocumento + '\'' +
                ", Nombre='" + nombre + '\'' +
                ", Apellido='" + apellido + '\'' +
                ", Edad=" + edad +
                ", Estado civil='" + estadoCivil + '\'' +
                ", Telefono='" + telefono + '\'' +
                ", Email='" + email;
    }
}
