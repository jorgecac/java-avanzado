package tarea.clase.tres.constantes;

public class EstadoCivil {

    public static final String SOLTERO = "S";
    public static final String CASADO = "C";
    public static final String VIUDO = "V";
    public static final String DIVORCIADO = "D";
}
