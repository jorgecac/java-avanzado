package tarea.clase.tres.constantes;

public class TipoPrestamo {
    //consumo, vivienda, estudiantil o universitario)

    public static final String CONSUMO = "consumo";
    public static final String VIVIENDA = "vivienda";
    public static final String ESTUDIANTIL = "estudiantil";
    public static final String UNIVERSITARIO = "universitario";
}
