package tarea.clase.tres.utils;

import tarea.clase.tres.constantes.EstadoCivil;
import tarea.clase.tres.constantes.Moneda;
import tarea.clase.tres.constantes.TipoPrestamo;

import java.util.Scanner;

/**
 * Created by jcaceresf on 8/23/20
 */
public class PrestamoUtil {

    public static String obtenerTipoPrestamo() throws Exception {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese el tipo de prestamo: ");
        System.out.println("1 - Consumo\n 2 - Vivienda\n3 - Estudiantil\n4 - Universitario");
        int tipoPrestamo = scanner.nextInt();
        switch (tipoPrestamo) {
            case 1:
                return TipoPrestamo.CONSUMO;
            case 2:
                return TipoPrestamo.VIVIENDA;
            case 3:
                return TipoPrestamo.ESTUDIANTIL;
            case 4:
                return TipoPrestamo.UNIVERSITARIO;
            default:
                throw new Exception("Tipo de prestamo no valido");
        }
    }

    public static String obtenerMoneda() throws Exception {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese la moneda del prestamo: ");
        System.out.println("1 - Guaranies\n 2 - Dolares");
        int moneda = scanner.nextInt();
        switch (moneda) {
            case 1:
                return Moneda.GUARANIES;
            case 2:
                return Moneda.DOLARES;
            default:
                throw new Exception("Moneda no valida");
        }
    }


    public static String obtenerEstadoCivil() throws Exception {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese el estado civil de la persona: ");
        System.out.println("1 - Soltero\n 2 - Casado\n3 - Viudo\n4 - Divorciado");
        int estadoCivil = scanner.nextInt();
        switch (estadoCivil) {
            case 1:
                return EstadoCivil.SOLTERO;
            case 2:
                return EstadoCivil.CASADO;
            case 3:
                return EstadoCivil.VIUDO;
            case 4:
                return EstadoCivil.DIVORCIADO;
            default:
                throw new Exception("Estado civil no valido");
        }
    }
}
