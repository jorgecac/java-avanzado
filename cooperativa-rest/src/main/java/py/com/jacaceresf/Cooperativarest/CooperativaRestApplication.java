package py.com.jacaceresf.Cooperativarest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CooperativaRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CooperativaRestApplication.class, args);
	}

}
