package py.com.jacaceresf.Cooperativarest.service;

import org.springframework.stereotype.Service;
import py.com.jacaceresf.Cooperativarest.beans.Cuenta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcaceresf on 11/8/20
 */
@Service
public class CooperativaServiceImpl implements CooperativaService {

    private List<Cuenta> cuentas = new ArrayList<>();

    @Override
    public List<Cuenta> getAccounts() {
        return cuentas;
    }

    @Override
    public Cuenta getAccountByNumber(String nroCuenta) {
        return cuentas.stream().filter(cuenta -> cuenta.getNroCuenta().equals(nroCuenta)).findFirst().orElse(null);
    }

    @Override
    public Cuenta createAccount(Cuenta cuenta) {
        cuentas.add(cuenta);
        return cuenta;
    }

    @Override
    public boolean deleteAccount(String nroCuenta) {
        return cuentas.removeIf(cuenta -> cuenta.getNroCuenta().equals(nroCuenta));
    }

    @Override
    public Cuenta updateAccount(Cuenta cuenta) {

        if (cuentas.stream().anyMatch(cuentaGuardada -> cuentaGuardada.getNroCuenta().equals(cuenta.getNroCuenta()))) {

            cuentas.forEach(cuentaGuardada -> {
                if (cuentaGuardada.getNroCuenta().equals(cuenta.getNroCuenta())) {
                    cuentaGuardada.setNroCuenta(cuenta.getNroCuenta());
                    cuentaGuardada.setEstado(cuenta.getEstado());
                    cuentaGuardada.setMoneda(cuenta.getMoneda());
                    cuentaGuardada.setNroSocio(cuenta.getNroSocio());
                    cuentaGuardada.setSaldo(cuenta.getSaldo());
                    cuentaGuardada.setTipoCuenta(cuenta.getTipoCuenta());
                }
            });
            return cuenta;
        } else {
            return null;
        }
    }
}
