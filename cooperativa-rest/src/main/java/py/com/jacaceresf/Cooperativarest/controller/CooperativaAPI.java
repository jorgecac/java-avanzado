package py.com.jacaceresf.Cooperativarest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import py.com.jacaceresf.Cooperativarest.beans.Cuenta;
import py.com.jacaceresf.Cooperativarest.service.CooperativaService;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by jcaceresf on 11/8/20
 */
@RestController
@RequestMapping(value = "/cuentas-cooperativa")
public class CooperativaAPI {

    @Autowired
    private CooperativaService cooperativaService;

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<Cuenta> getAccounts() {
        return cooperativaService.getAccounts();
    }

    @GetMapping(value = "/{nroCuenta}", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody
    Cuenta getAccountById(@PathVariable(value = "nroCuenta") String nroCuenta, HttpServletResponse response) {

        Cuenta accountByNumber = cooperativaService.getAccountByNumber(nroCuenta);

        if (accountByNumber != null)
            response.setStatus(HttpStatus.OK.value());
        else
            response.setStatus(HttpStatus.NOT_FOUND.value());

        return accountByNumber;
    }

    @PostMapping(value = "/guardar", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public @ResponseBody
    Cuenta saveAccount(@RequestBody Cuenta cuenta) {
        return cooperativaService.createAccount(cuenta);
    }

    @PatchMapping(value = "/modificar", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public @ResponseBody
    Cuenta updateAccount(@RequestBody Cuenta cuenta, HttpServletResponse response) {

        Cuenta cuentaModificada = cooperativaService.updateAccount(cuenta);

        if (cuentaModificada != null)
            response.setStatus(HttpStatus.OK.value());
        else
            response.setStatus(HttpStatus.NOT_FOUND.value());

        return cuentaModificada;
    }

    @DeleteMapping(value = "/eliminar/{nroCuenta}", produces = APPLICATION_JSON_VALUE)
    public @ResponseBody
    void deleteAccount(@PathVariable(value = "nroCuenta") String nroCuenta, HttpServletResponse response) {

        if (cooperativaService.deleteAccount(nroCuenta)) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
        } else {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }

    }

}
