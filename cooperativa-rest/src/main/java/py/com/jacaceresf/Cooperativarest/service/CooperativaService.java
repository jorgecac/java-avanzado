package py.com.jacaceresf.Cooperativarest.service;

import py.com.jacaceresf.Cooperativarest.beans.Cuenta;

import java.util.List;

/**
 * Created by jcaceresf on 11/8/20
 */
public interface CooperativaService {

    List<Cuenta> getAccounts();

    Cuenta getAccountByNumber(String nroCuenta);

    Cuenta createAccount(Cuenta cuenta);

    boolean deleteAccount(String nroCuenta);

    Cuenta updateAccount(Cuenta cuenta);

}
