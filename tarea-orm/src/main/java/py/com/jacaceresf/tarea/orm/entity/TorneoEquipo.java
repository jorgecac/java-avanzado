package py.com.jacaceresf.tarea.orm.entity;

import javax.persistence.*;

/**
 * Created by jcaceresf on 11/22/20
 */
@Entity
@Table(name = "torneo_equipos")
public class TorneoEquipo {

    @EmbeddedId
    private TorneoEquipoPK torneoEquipoPK;

    @MapsId("torneo.id")
    @ManyToOne(optional = false)
    @JoinColumn(name = "id", referencedColumnName = "id")
    private Torneo torneo;

    @MapsId("equipo.id")
    @ManyToOne(optional = false)
    @JoinColumn(name = "id", referencedColumnName = "id")
    private Equipo equipo;

    public TorneoEquipoPK getTorneoEquipoPK() {
        return torneoEquipoPK;
    }

    public void setTorneoEquipoPK(TorneoEquipoPK torneoEquipoPK) {
        this.torneoEquipoPK = torneoEquipoPK;
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    @Override
    public String toString() {
        return "TorneoEquipo{" +
                "torneo=" + torneo +
                ", equipo=" + equipo +
                '}';
    }
}
