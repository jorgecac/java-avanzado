package py.com.jacaceresf.tarea.orm.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by jcaceresf on 11/22/20
 */
@Embeddable
public class TorneoEquipoPK implements Serializable {

    @Column(name = "torneo_id")
    private Long torneoId;
    @Column(name = "equipo_id")
    private Long equipoId;

    public Long getTorneoId() {
        return torneoId;
    }

    public void setTorneoId(Long torneoId) {
        this.torneoId = torneoId;
    }

    public Long getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(Long equipoId) {
        this.equipoId = equipoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TorneoEquipoPK that = (TorneoEquipoPK) o;
        return Objects.equals(torneoId, that.torneoId) &&
                Objects.equals(equipoId, that.equipoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(torneoId, equipoId);
    }
}
