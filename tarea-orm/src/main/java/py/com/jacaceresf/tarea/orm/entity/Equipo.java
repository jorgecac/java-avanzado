package py.com.jacaceresf.tarea.orm.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jcaceresf on 11/22/20
 */
@Entity
@Table(name = "equipo")
public class Equipo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "promocion")
    private String promocion;

    @Column(name = "url_imagen_equipo")
    private String urlImagenEquipo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setPromocion(String promocion) {
        this.promocion = promocion;
    }

    public String getUrlImagenEquipo() {
        return urlImagenEquipo;
    }

    public void setUrlImagenEquipo(String urlImagenEquipo) {
        this.urlImagenEquipo = urlImagenEquipo;
    }

    @Override
    public String toString() {
        return "Equipo{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", promocion='" + promocion + '\'' +
                ", urlImagenEquipo='" + urlImagenEquipo + '\'' +
                '}';
    }
}
