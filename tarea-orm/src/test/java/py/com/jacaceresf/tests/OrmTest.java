package py.com.jacaceresf.tests;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Before;
import org.junit.Test;
import py.com.jacaceresf.tarea.orm.entity.Equipo;
import py.com.jacaceresf.tarea.orm.entity.Torneo;
import py.com.jacaceresf.tarea.orm.entity.TorneoEquipo;
import py.com.jacaceresf.tarea.orm.entity.TorneoEquipoPK;

import java.util.Date;
import java.util.List;

/**
 * Created by jcaceresf on 11/22/20
 */
public class OrmTest {

    private Session session;

    @Before
    public void init() {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        session = sessionFactory.openSession();
    }

    @Test
    public void testEquipo() {
        session.beginTransaction();
        Equipo equipo = new Equipo();
        equipo.setNombre("Liverpool FC");
        equipo.setPromocion("2020");
        equipo.setUrlImagenEquipo("http://r;wjn;lkj;lkjlkj");
        session.save(equipo);
        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testTorneo() {
        session.beginTransaction();

        Torneo torneo = new Torneo();
        torneo.setFechaInicio(new Date());
        torneo.setNombre("Torneito 3");
        torneo.setTemporada("Segunda temporada");

        session.save(torneo);
        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void testTorneoEquipo() {

        session.beginTransaction();

        TorneoEquipoPK torneoEquipoPK = new TorneoEquipoPK();
        torneoEquipoPK.setEquipoId(1L);
        torneoEquipoPK.setTorneoId(1L);

        TorneoEquipo torneoEquipo = new TorneoEquipo();
        torneoEquipo.setTorneoEquipoPK(torneoEquipoPK);

        session.save(torneoEquipo);

        session.getTransaction().commit();
        session.close();
    }

    @Test
    public void getTest() {
        Query query = session.createQuery("from Equipo");
        List<Equipo> list = query.list();
        list.forEach(System.out::println);

        Query torneoQry = session.createQuery("from Torneo");
        List<Torneo> listTorneo = torneoQry.list();
        listTorneo.forEach(System.out::println);


    }
}
