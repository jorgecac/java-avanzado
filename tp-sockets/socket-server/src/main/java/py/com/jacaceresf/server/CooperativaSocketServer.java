package py.com.jacaceresf.server;

/**
 * Created by jcaceresf on 10/7/20
 */
public class CooperativaSocketServer {

    private static CooperativaServer socketServer;
    private static int SOCKET_PORT = 8081;

    public static void main(String[] args) {
        socketServer = new CooperativaServer(SOCKET_PORT);
    }

}
