package py.com.jacaceresf.server;

import py.com.jacaceresf.beans.Aporte;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jcaceresf on 10/4/20
 */
public class GenericOperations<E> {

    public <E extends Comparable<E>> int getCounterByComparableValue(List<E> list, Object valueToCompare) {

        int counter = 0;
        for (E element : list) {
            if (element.equals(valueToCompare)) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * - Si recibe “3 - <NumeroSocio> - <NumeroCuenta> - <AnhoMesAporte>” debe retornar la cantidad de aportes
     * que fueron eliminados de la colección de acuerdo con el número de socio, numero de cuenta y año/mes del aporte.
     * (Para esto deberá crear un método genérico).
     *
     * @param list
     * @param valueToCompare
     * @param <E>
     * @return
     */
    public <E extends Comparable<E>> String deleteElements(Collection<E> list, Object valueToCompare) {

        int deletedElements = 0;

        Iterator<E> iterator = list.iterator();

        while (iterator.hasNext()) {
            if (iterator.next().equals(valueToCompare)) {
                deletedElements++;
                iterator.remove();
            }
        }
        return String.valueOf(deletedElements);
    }

    /**
     * * 2. Recibir solicitudes para modificar (2) datos de los socios de la Cooperativa de la siguiente manera:
     * * <p>
     * * - Si recibe “2 - <NumeroSocio> - <NumeroCuenta> - <AnhoMesAporte> - <MontoAporte> - <FechaAporte> - <NombreAgencia>”
     * * <p>
     * * Se debe actualizar el monto del aporte, la fecha del aporte y el nombre de la agencia en el elemento de la colección de acuerdo con el número de socio,
     * * número de cuenta y año/mes aporte. (Para esto deberá crear un método genérico).
     * *
     *
     * @param collection
     * @param valueToCompare
     * @return
     */
    public String updateElements(Collection<Aporte> collection, Aporte valueToCompare) {

        Iterator iterator = collection.iterator();

        boolean beenUpdated = false;

        while (iterator.hasNext()) {
            Object object = iterator.next();
            if (object.equals(valueToCompare)) {

                Aporte update = (Aporte) object;
                update.setMontoAporte(valueToCompare.getMontoAporte());
                update.setFechaAporte(valueToCompare.getFechaAporte());
                update.setNombreAgencia(valueToCompare.getNombreAgencia());
                beenUpdated = true;
            }
        }

        return beenUpdated ? "Se modifico el aporte del socio " + valueToCompare.getNumeroSocio() : "No se modifico ningun aporte.";

    }

}
