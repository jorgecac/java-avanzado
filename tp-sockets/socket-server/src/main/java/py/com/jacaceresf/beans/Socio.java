package py.com.jacaceresf.beans;

import sun.reflect.generics.scope.Scope;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by jcaceresf on 10/4/20
 */
public class Socio {

    public static final String DATE_PATTERN = "dd/MM/yyyy";

    private int numeroSocio;
    private String nombreSocio;
    private Aporte aporte;

    /**
     * <NumeroSocio> - <NombreSocio> - <NumeroCuenta> - <AnhoMesAporte> -
     * <MontoAporte> - <FechaAporte> - <NombreAgencia>
     *
     * @param parameters
     * @return
     */
    public static Socio buildSocioFromData(String[] parameters) {
        Socio socio = new Socio();
        socio.setAporte(new Aporte());
        socio.setNumeroSocio(Integer.parseInt(parameters[1]));
        socio.setNombreSocio(parameters[2]);
        socio.getAporte().setNumeroSocio(socio.getNumeroSocio());
        socio.getAporte().setNumeroCuenta(Integer.parseInt(parameters[3]));
        socio.getAporte().setAnhoMesAporte(Integer.parseInt(parameters[4]));
        socio.getAporte().setMontoAporte(new BigDecimal(parameters[5]));
        socio.getAporte().setFechaAporte(LocalDate.parse(parameters[6], DateTimeFormatter.ofPattern(DATE_PATTERN)));
        socio.getAporte().setNombreAgencia(parameters[7]);
        return socio;
    }

    public int getNumeroSocio() {
        return numeroSocio;
    }

    public void setNumeroSocio(int numeroSocio) {
        this.numeroSocio = numeroSocio;
    }

    public String getNombreSocio() {
        return nombreSocio;
    }

    public void setNombreSocio(String nombreSocio) {
        this.nombreSocio = nombreSocio;
    }

    public Aporte getAporte() {
        return aporte;
    }

    public void setAporte(Aporte aporte) {
        this.aporte = aporte;
    }

    @Override
    public String toString() {
        return "Socio [" +
                "numeroSocio=" + numeroSocio +
                ", nombreSocio='" + nombreSocio + '\'' +
                ", aporte=" + aporte +
                ']';
    }
}
