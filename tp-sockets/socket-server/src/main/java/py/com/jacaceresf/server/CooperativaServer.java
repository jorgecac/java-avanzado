package py.com.jacaceresf.server;

import py.com.jacaceresf.beans.Aporte;
import py.com.jacaceresf.beans.Socio;

import java.io.*;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static py.com.jacaceresf.beans.Socio.DATE_PATTERN;

/**
 * Created by jcaceresf on 10/4/20
 */
public class CooperativaServer {

    private Map<Integer, Socio> sociosMap = new ConcurrentHashMap<Integer, Socio>();
    private List<Aporte> aportesList = Collections.synchronizedList(new ArrayList<>());
    private GenericOperations<Aporte> aporteGenericOperations = new GenericOperations<>();

    private static final String SEPARATOR_CHAR = " - ";

    private boolean serverListening = true;
    private boolean closeServer = false;

    private int serverPort;
    private Socket service;
    private ServerSocket socketService;
    private DataOutputStream outputData;
    private DataInputStream inputData;

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public CooperativaServer(int serverPort) {
        this.serverPort = serverPort;
        this.connection();
    }

    private void connection() {

        try {
            socketService = new ServerSocket(this.serverPort);
            service = socketService.accept();
            System.out.printf("##### El servidor esta escuchando en el puerto => [%d] #####\n", this.serverPort);

            Thread process = new Thread(new Runnable() {
                @Override
                public void run() {
                    receiveData();
                }
            });

            process.start();

        } catch (IOException e) {
            System.out.println("Ocurrio un error al establecer la conexion del socket => " + e.getMessage());
        }


    }

    private synchronized void receiveData() {
        while (serverListening) {
            try {
                InputStream inputStream = service.getInputStream();

                inputData = new DataInputStream(inputStream);
                String receivedData = inputData.readUTF();

                System.out.println("PARAMETRO RECIBIDO => [" + receivedData + "]");
                String[] receivedParameters = receivedData.split(SEPARATOR_CHAR);

                this.executeRequest(receivedParameters);

            } catch (IOException ex) {
                try {
                    if (service != null) {
                        service.close();
                    }

                    if (inputData != null) {
                        inputData.close();
                    }
                    service = socketService.accept();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendResultToClient(String result) {
        try {
            OutputStream outputStream = service.getOutputStream();
            outputData = new DataOutputStream(outputStream);
            outputData.writeUTF(result);
            outputData.flush();
            if (closeServer) {
                this.shutdownServer();
                this.serverListening = false;
            }
        } catch (IOException ex) {
            System.out.println("IOException while trying to send result to client => " + ex);
            ex.printStackTrace();
        }
    }

    private void shutdownServer() {
        try {
            if (outputData != null)
                outputData.close();
            if (inputData != null)
                inputData.close();
            if (socketService != null) {
                socketService.close();
                System.out.printf("#### El servidor en el puerto [%d] esta abajo.#####\n", this.getServerPort());
            }
            if (service != null)
                service.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void executeRequest(String[] parameters) {

        String result = null;
        if (checkRequest(parameters[0], parameters.length)) {
            switch (parameters[0]) {
                case "1":
                    result = addSocio(parameters);
                    break;
                case "2":
                    result = update(parameters);
                    break;
                case "3":
                    result = deleteAporte(parameters);
                    break;
                case "4":
                    result = query(parameters);
                    break;
            }
        } else {
            result = "Respuesta del Servidor: Cantidad de parametros recibidos incorrectos\n";
        }
        this.sendResultToClient(result);
    }


    private boolean checkRequest(String request, int parametersCounter) {
        if (request.equals("1") && parametersCounter == 8) {
            return true;
        } else if (request.equals("2") && parametersCounter == 7) {
            return true;
        } else if (request.equals("3") && parametersCounter == 4) {
            return true;
        } else return request.equals("4") && parametersCounter == 3;
    }

    /**
     * Metodo para agregar un socio al mapa de socios con sus aportes.
     *
     * @param parameters
     * @return
     */
    private String addSocio(String[] parameters) {
        Socio socio = Socio.buildSocioFromData(parameters);
        for (Map.Entry<Integer, Socio> entry : sociosMap.entrySet()) {
            if (entry.getValue() != null) {
                if (entry.getValue().getAporte().getNumeroCuenta() == socio.getAporte().getNumeroCuenta()
                        && entry.getValue().getAporte().getAnhoMesAporte() == socio.getAporte().getAnhoMesAporte()
                        && entry.getValue().getAporte().getFechaAporte() == socio.getAporte().getFechaAporte()
                        && entry.getValue().getAporte().getNombreAgencia().equals(socio.getAporte().getNombreAgencia())) {
                    return String.format("El socio con el aporte ya se encuentra registrado => %s", socio.toString());
                }
            }
        }

        sociosMap.put(socio.getNumeroSocio(), socio);
        aportesList.add(socio.getAporte());
        return String.format("Socio agregado => %s", socio.toString());
    }

    /**
     * 2. Recibir solicitudes para modificar (2) datos de los socios de la Cooperativa de la siguiente manera:
     * <p>
     * - Si recibe “2 - <NumeroSocio> - <NumeroCuenta> - <AnhoMesAporte> - <MontoAporte> - <FechaAporte> - <NombreAgencia>”
     * <p>
     * Se debe actualizar el monto del aporte, la fecha del aporte y el nombre de la agencia en el elemento de la colección de acuerdo con el número de socio,
     * número de cuenta y año/mes aporte. (Para esto deberá crear un método genérico).
     *
     * @param parameters
     * @return
     */
    private String update(String[] parameters) {

        Aporte aporte = new Aporte();
        aporte.setNumeroSocio(Integer.parseInt(parameters[1]));
        aporte.setNumeroCuenta(Integer.parseInt(parameters[2]));
        aporte.setAnhoMesAporte(Integer.parseInt(parameters[3]));
        aporte.setMontoAporte(new BigDecimal(parameters[4]));
        aporte.setFechaAporte(LocalDate.parse(parameters[5], DateTimeFormatter.ofPattern(DATE_PATTERN)));
        aporte.setNombreAgencia(parameters[6]);

        return aporteGenericOperations.updateElements(aportesList, aporte);
    }

    /**
     * - Si recibe “4 - 1 - <NumeroSocio>” debe buscar y retornar todos los aportes
     * que fueron depositados por el socio de acuerdo con el número de socio ingresado. (Para esto deberá utilizar StreamFilters)
     *
     * @param nroSocio
     * @return
     */
    private String getAportesBySocio(int nroSocio) {

        System.out.println(aportesList);

        List<Aporte> aportesBySocio = aportesList.parallelStream()
                .filter(aporte -> aporte.getNumeroSocio() == nroSocio)
                .collect(Collectors.toList());
        System.out.println("CONSULTAMOS LOS APORTES");
        String result = "";
        if (!aportesBySocio.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            aportesBySocio.forEach(aporte -> sb.append(String.format("%s \n", aporte.toString())));
            result = sb.toString();
        } else {
            result = String.format("El socio %d no tiene aportes.", nroSocio);
        }

        System.out.println(result);
        return result;
    }

    private String query(String[] parameters) {
        String result = null;
        if (parameters[1].equals("1")) {
            System.out.println("Vamos a retornar los aportes del socio =>" + parameters[2]);
            result = getAportesBySocio(Integer.parseInt(parameters[2]));
        } else if (parameters[1].equals("2")) {

            Aporte aporte = new Aporte();
            aporte.setNombreAgencia(parameters[2]);
            System.out.println("Vamos a consultar todos los aportes de la agencia => " + aporte.getNombreAgencia());

            result = String.format("Hay %d aportes para la agencia %s", aporteGenericOperations.getCounterByComparableValue(aportesList, aporte), aporte.getNombreAgencia());
        }
        return result;
    }

    private String deleteAporte(String[] parameters) {
        Aporte aporte = new Aporte();
        aporte.setNumeroSocio(Integer.parseInt(parameters[1]));
        aporte.setNumeroCuenta(Integer.parseInt(parameters[2]));
        aporte.setAnhoMesAporte(Integer.parseInt(parameters[3]));
        return String.format("Se eliminaron %s aportes correspondientes al socio %d", aporteGenericOperations.deleteElements(aportesList, aporte), aporte.getNumeroSocio());
    }


}
