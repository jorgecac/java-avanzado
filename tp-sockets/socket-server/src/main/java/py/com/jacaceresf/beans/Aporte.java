package py.com.jacaceresf.beans;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by jcaceresf on 10/4/20
 */
public class Aporte implements Comparable {

    private int numeroSocio;
    private int numeroCuenta;
    private int anhoMesAporte;
    private BigDecimal montoAporte;
    private LocalDate fechaAporte;
    private String nombreAgencia;

    public Aporte() {
        this.numeroCuenta = 0;
        this.numeroSocio = 0;
        this.anhoMesAporte = 0;
    }

    public int getNumeroSocio() {
        return numeroSocio;
    }

    public void setNumeroSocio(int numeroSocio) {
        this.numeroSocio = numeroSocio;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public int getAnhoMesAporte() {
        return anhoMesAporte;
    }

    public void setAnhoMesAporte(int anhoMesAporte) {
        this.anhoMesAporte = anhoMesAporte;
    }

    public BigDecimal getMontoAporte() {
        return montoAporte;
    }

    public void setMontoAporte(BigDecimal montoAporte) {
        this.montoAporte = montoAporte;
    }

    public LocalDate getFechaAporte() {
        return fechaAporte;
    }

    public void setFechaAporte(LocalDate fechaAporte) {
        this.fechaAporte = fechaAporte;
    }

    public String getNombreAgencia() {
        return nombreAgencia;
    }

    public void setNombreAgencia(String nombreAgencia) {
        this.nombreAgencia = nombreAgencia;
    }

    @Override
    public String toString() {
        return "Aporte [" +
                "numeroSocio=" + numeroSocio +
                ", numeroCuenta=" + numeroCuenta +
                ", anhoMesAporte=" + anhoMesAporte +
                ", montoAporte=" + montoAporte +
                ", fechaAporte=" + fechaAporte +
                ", nombreAgencia='" + nombreAgencia + '\'' +
                ']';
    }

    @Override
    public boolean equals(Object o) {
        Aporte aporte = (Aporte) o;
        if (aporte.getNombreAgencia() != null && aporte.getNombreAgencia().equals(this.nombreAgencia)) {
            return true;
        } else
            return this.numeroSocio == aporte.numeroSocio && this.numeroCuenta == aporte.numeroCuenta && this.anhoMesAporte == aporte.anhoMesAporte;

    }


    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
