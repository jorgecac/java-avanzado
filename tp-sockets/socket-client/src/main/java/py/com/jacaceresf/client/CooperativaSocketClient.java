package py.com.jacaceresf.client;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by jcaceresf on 10/7/20
 */
public class CooperativaSocketClient {

    private Socket socketClient = null;
    private BufferedReader input = null;
    private DataInputStream inputData = null;
    private DataOutputStream outputData = null;
    private int portNumber;
    private String ip;

    private final String LINE_SEPARATOR = "#######################################";

    public int getPortNumber() {
        return portNumber;
    }

    public String getIp() {
        return ip;
    }

    public CooperativaSocketClient(int portNumber, String ip) {
        this.portNumber = portNumber;
        this.ip = ip;
        this.connection();
    }

    private void connection() {
        try {
            socketClient = new Socket(this.getIp(), this.getPortNumber());

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    inputData();
                }
            });

            thread.start();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void inputData() {
        int opcion = 0;
        try {
            while (opcion < 5) {

                input = new BufferedReader(new InputStreamReader(System.in));

                System.out.println(LINE_SEPARATOR);
                System.out.println("Socket de Cooperativa.");
                System.out.println("Cuenta con las siguientes opciones:");
                System.out.println(LINE_SEPARATOR);
                System.out.println("1 - Agregar datos de un socio y su aporte.");
                System.out.println("2 - Modificar datos de un socio.");
                System.out.println("3 - Eliminar aportes de 1un socio.");
                System.out.println("4 - Consulta de datos.");
                System.out.println(LINE_SEPARATOR);
                try {
                    opcion = Integer.parseInt(input.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                    opcion = 5;
                }

                switch (opcion) {

                    case 1:
                        addSocio();
                        break;
                    case 2:
                        updateSocio();
                        break;
                    case 3:
                        deleteSocio();
                        break;
                    case 4:
                        query();
                        break;
                }

                listen(socketClient);
                Thread.sleep(1500);

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.closeAllResources();
    }

    private boolean listen(Socket socket) throws IOException {
        InputStream inputStream = null;
        String stringReceived = null;
        inputStream = socket.getInputStream();
        inputData = new DataInputStream(inputStream);
        stringReceived = inputData.readUTF();
        System.out.println("Respuesta del socket => [" + stringReceived + "]");
        return false;
    }

    private void sendDataToServer(String data) {

        try {
            OutputStream outputStream = socketClient.getOutputStream();
            outputData = new DataOutputStream(outputStream);
            outputData.writeUTF(data);
            outputData.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    /**
     * 1 - <NumeroSocio> - <NombreSocio> - <NumeroCuenta> - <AnhoMesAporte> - <MontoAporte> - <FechaAporte> - <NombreAgencia>
     *
     * @throws IOException
     */
    private void addSocio() throws IOException {

        String nroSocio, nombre, nroCuenta, anhoMesAporte, montoAporte, fechaAporte, nombreAgencia;
        System.out.println("Ingrese el numero de socio: ");
        nroSocio = input.readLine();

        System.out.println("Ingrese el nombre del socio: ");
        nombre = input.readLine();

        System.out.println("Ingrese el numero de cuenta: ");
        nroCuenta = input.readLine();

        System.out.println("Ingrese el anho y mes de aporte: ");
        anhoMesAporte = input.readLine();

        System.out.println("Ingrese el monto de aporte: ");
        montoAporte = input.readLine();

        System.out.println("Ingrese el la fecha de aporte DD/MM/YYYY: ");
        fechaAporte = input.readLine();

        System.out.println("Ingrese el nombre de la agencia: ");
        nombreAgencia = input.readLine();

        String addSocioRequest = String.format("1 - %s - %s - %s - %s - %s - %s - %s",
                nroSocio, nombre, nroCuenta, anhoMesAporte, montoAporte, fechaAporte, nombreAgencia
        );

        System.out.println("Datos a enviar para agregar el socio => [" + addSocioRequest + "]");

        sendDataToServer(addSocioRequest);
    }

    /**
     * 2 - <NumeroSocio> - <NumeroCuenta> - <AnhoMesAporte> - <MontoAporte> - <FechaAporte> - <NombreAgencia>
     */
    private void updateSocio() throws IOException {
        String nroSocio, nroCuenta, anhoMesAporte, montoAporte, fechaAporte, nombreAgencia;

        System.out.println("Ingrese el numero de socio: ");
        nroSocio = input.readLine();

        System.out.println("Ingrese el numero de cuenta: ");
        nroCuenta = input.readLine();

        System.out.println("Ingrese el anho y mes de aporte: ");
        anhoMesAporte = input.readLine();

        System.out.println("Ingrese el monto de aporte: ");
        montoAporte = input.readLine();

        System.out.println("Ingrese el la fecha de aporte DD/MM/YYYY: ");
        fechaAporte = input.readLine();

        System.out.println("Ingrese el nombre de la agencia: ");
        nombreAgencia = input.readLine();

        String updateSocioRequest = String.format("2 - %s - %s - %s - %s - %s - %s",
                nroSocio, nroCuenta, anhoMesAporte, montoAporte, fechaAporte, nombreAgencia
        );

        System.out.println("Datos a enviar para modificar el socio => [" + updateSocioRequest + "]");

        sendDataToServer(updateSocioRequest);
    }

    /**
     * 3 - <NumeroSocio> - <NumeroCuenta> - <AnhoMesAporte>
     */
    private void deleteSocio() throws IOException {
        String nroSocio, nroCuenta, anhoMesAporte;

        System.out.println("Ingrese el numero de socio: ");
        nroSocio = input.readLine();

        System.out.println("Ingrese el numero de cuenta: ");
        nroCuenta = input.readLine();

        System.out.println("Ingrese el anho y mes de aporte: ");
        anhoMesAporte = input.readLine();

        String deleteSocioRequest = String.format("3 - %s - %s - %s",
                nroSocio, nroCuenta, anhoMesAporte
        );

        System.out.println("Datos a enviar para eliminar el socio => [" + deleteSocioRequest + "]");
        sendDataToServer(deleteSocioRequest);
    }

    /**
     * 4 - 1 - <NumeroSocio>
     * <p>
     * 4 - 2 - <NombreAgencia>
     */
    private void query() throws IOException {
        System.out.println("4 - Ingrese una opcion: ");
        System.out.println("1 - Para consultar por todos los aportes del socio");
        System.out.println("2 - Para consultar por todos los aportes depositados en la agencia");

        int opcion = Integer.parseInt(input.readLine());

        String dato = "";
        if (opcion == 1) {
            System.out.println("Ingrese el nro. de socio: ");
            dato = input.readLine();
        } else if (opcion == 2) {
            System.out.println("Ingrese el nombre de la agencia: ");
            dato = input.readLine();
        }

        String query = String.format("4 - %d - %s",
                opcion, dato
        );
        System.out.println("Dato a enviar para consulta => [" + query + "]");
        sendDataToServer(query);
    }

    private void closeAllResources() {
        try {

            if (outputData != null)
                outputData.close();
            if (inputData != null)
                inputData.close();
            if (socketClient != null) {
                socketClient.close();
                System.out.println(LINE_SEPARATOR);
                System.out.println("El socket se ha cerrado.");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
