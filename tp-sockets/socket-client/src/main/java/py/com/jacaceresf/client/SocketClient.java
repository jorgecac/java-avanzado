package py.com.jacaceresf.client;

/**
 * Created by jcaceresf on 10/7/20
 */
public class SocketClient {

    private static CooperativaSocketClient cooperativaSocketClient;
    private static final int SERVER_PORT = 8081;
    private static final String HOST = "localhost";

    public static void main(String[] args) {
        cooperativaSocketClient = new CooperativaSocketClient(SERVER_PORT, HOST);
    }

}
