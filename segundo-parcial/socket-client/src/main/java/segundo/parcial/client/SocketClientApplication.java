package segundo.parcial.client;

/**
 * Created by jcaceresf on 10/17/20
 */
public class SocketClientApplication {

    private static SocketClient socketClient;
    private static final int SERVER_PORT = 8081;
    private static final String HOST = "localhost";

    public static void main(String[] args) {
        socketClient = new SocketClient(SERVER_PORT, HOST);
    }


}
