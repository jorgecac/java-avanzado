package segundo.parcial.client;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by jcaceresf on 10/17/20
 */
public class SocketClient {

    private Socket socketClient = null;
    private BufferedReader input = null;
    private DataInputStream inputData = null;
    private DataOutputStream outputData = null;
    private int portNumber;
    private String ip;

    private final String LINE_SEPARATOR = "#######################################";


    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public SocketClient(int portNumber, String ip) {
        this.portNumber = portNumber;
        this.ip = ip;
        this.connection();
    }

    private void connection() {
        try {
            socketClient = new Socket(this.getIp(), this.getPortNumber());
            Thread thread = new Thread(this::inputData);
            thread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void inputData() {
        int opcion = 0;
        try {
            while (opcion < 5) {

                input = new BufferedReader(new InputStreamReader(System.in));

                System.out.println(LINE_SEPARATOR);
                System.out.println("Socket de datos meteorologicos.");
                System.out.println("Cuenta con las siguientes opciones:");
                System.out.println(LINE_SEPARATOR);
                System.out.println("1 - Agregar los datos meteorologicos de una fecha.");
                System.out.println("2 - Modificar los datos de una estacion.");
                System.out.println("3 - Eliminar los datos de una estacion.");
                System.out.println("4 - Consultar datos meteorologicos.");
                System.out.println(LINE_SEPARATOR);
                try {
                    opcion = Integer.parseInt(input.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                    opcion = 5;
                }

                switch (opcion) {
                    case 1:
                    case 2:
                        addOrUpdateData(opcion);
                        break;
                    case 3:
                        delete();
                        break;
                    case 4:
                        query();
                        break;
                }

                listen(socketClient);
                Thread.sleep(1500);
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        this.closeAllResources();
    }

    private void listen(Socket socket) throws IOException {
        InputStream inputStream = null;
        String stringReceived = null;
        inputStream = socket.getInputStream();
        inputData = new DataInputStream(inputStream);
        stringReceived = inputData.readUTF();
        System.out.println("Respuesta del socket => [" + stringReceived + "]");
    }

    private void sendDataToServer(String data) {
        try {
            OutputStream outputStream = socketClient.getOutputStream();
            outputData = new DataOutputStream(outputStream);
            outputData.writeUTF(data);
            outputData.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void closeAllResources() {
        try {
            if (outputData != null)
                outputData.close();
            if (inputData != null)
                inputData.close();
            if (socketClient != null) {
                socketClient.close();
                System.out.println(LINE_SEPARATOR);
                System.out.println("El socket se ha cerrado.");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void addOrUpdateData(int action) throws IOException {

        String seasonId, date, hour, maxTemp, minTemp, windVel, pressure, humidity, precipitation;

        seasonId = getInputSeason();

        date = getInputDate();

        hour = getInputHour();

        System.out.println("Ingrese la temperatura maxima");
        maxTemp = input.readLine();

        System.out.println("Ingrese la temperatura minima");
        minTemp = input.readLine();

        System.out.println("Ingrese la velocidad del viento");
        windVel = input.readLine();

        System.out.println("Ingrese la presion");
        pressure = input.readLine();

        System.out.println("Ingrese la humedad");
        humidity = input.readLine();

        System.out.println("Ingrese la precipitacion");
        precipitation = input.readLine();

        String status = "w";
        String msg = "Datos a enviar para agregar los datos meteorologicos";
        String actionRequest = "A10";
        if (action == 2) {
            System.out.println("Ingrese el estado para modificar");
            status = input.readLine();
            actionRequest = "B11";
            msg = "Datos a enviar para modificar los datos meteorologicos";
        }

        String request = String.format("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s",
                actionRequest, seasonId, date, hour, maxTemp, minTemp, windVel, pressure, humidity, precipitation, status
        );

        System.out.printf("%s => [%s]\n", msg, request);

        sendDataToServer(request);
    }

    /**
     * D12 | <id estacion> | <fecha> | <hora>
     *
     * @throws IOException
     */
    private void delete() throws IOException {

        String season = getInputSeason();
        String date = getInputDate();
        String hour = getInputHour();

        String deleteRequest = String.format("D12 | %s | %s | %s", season, date, hour);

        System.out.println("Datos a enviar para eliminar los datos meteorologicos => [" + deleteRequest + "]");

        sendDataToServer(deleteRequest);
    }

    /**
     * C13 | 01 | <id_estacion> | <año>
     * C13 | 11 | <año>
     *
     * @throws IOException
     */
    private void query() throws IOException {
        System.out.println("4 - Ingrese una opcion: ");
        System.out.println("1 - Para consultar por todos los datos por año y estacion");
        System.out.println("2 - Para consultar por todos los datos por año");

        int opcion = Integer.parseInt(input.readLine());

        String request = null;

        String dato = "";
        if (opcion == 1) {
            request = String.format("C13 | 01 | %s | %s", getInputSeason(), getInputYear());
        } else if (opcion == 2) {
            request = String.format("C13 | 11 | %s", getInputYear());
        }

        System.out.println("Dato a enviar para consulta => [" + request + "]");
        sendDataToServer(request);
    }


    private String getInputDate() throws IOException {
        System.out.println("Ingrese la fecha en formato AAAAMMDD");
        return input.readLine();
    }

    private String getInputSeason() throws IOException {
        System.out.println("Ingrese el ID de la estacion: ");
        return input.readLine();
    }

    private String getInputHour() throws IOException {
        System.out.println("Ingrese la hora en formato HHMMSS");
        return input.readLine();
    }

    private String getInputYear() throws IOException {
        System.out.println("Ingrese el año");
        return input.readLine();
    }
}
