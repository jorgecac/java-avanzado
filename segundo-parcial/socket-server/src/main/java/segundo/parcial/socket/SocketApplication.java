package segundo.parcial.socket;

/**
 * Created by jcaceresf on 10/17/20
 */
public class SocketApplication {

    private static SocketServer socketServer;
    private static final int SERVER_PORT = 8081;

    public static void main(String[] args) {
        socketServer = new SocketServer(SERVER_PORT);
    }

}
