package segundo.parcial.socket;

import segundo.parcial.beans.WeatherData;
import segundo.parcial.utils.TimeUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by jcaceresf on 10/14/20
 */
public class SocketServer {

    private Map<String, WeatherData> weatherMap = new ConcurrentHashMap<>();

    private static final String SEPARATOR_CHAR = "(\\s)\\|(\\s)";


    private boolean serverListening = true;
    private boolean closeServer = false;

    private int serverPort;
    private Socket service;
    private ServerSocket socketService;
    private DataOutputStream outputData;
    private DataInputStream inputData;

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public SocketServer(int serverPort) {
        this.serverPort = serverPort;
        this.connection();
    }

    private void connection() {
        try {
            socketService = new ServerSocket(this.serverPort);
            service = socketService.accept();
            System.out.printf("##### El servidor esta escuchando en el puerto => [%d] #####\n", this.serverPort);
            Thread process = new Thread(this::receiveData);
            process.start();
        } catch (IOException e) {
            System.out.println("Ocurrio un error al establecer la conexion del socket => " + e.getMessage());
        }
    }

    private synchronized void receiveData() {
        while (serverListening) {
            try {
                InputStream inputStream = service.getInputStream();

                inputData = new DataInputStream(inputStream);
                String receivedData = inputData.readUTF();

                System.out.println("PARAMETRO RECIBIDO => [ " + receivedData + " ]");
                String[] receivedParameters = receivedData.split(SEPARATOR_CHAR);

                this.executeRequest(receivedParameters);

            } catch (IOException ex) {
                try {
                    if (service != null) {
                        service.close();
                    }

                    if (inputData != null) {
                        inputData.close();
                    }
                    service = socketService.accept();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void executeRequest(String[] parameters) {
        String result = null;
        try {
            if (checkRequest(parameters[0], parameters.length)) {
                switch (parameters[0]) {
                    case "A10":
                        result = add(parameters);
                        break;
                    case "B11":
                        result = update(parameters);
                        break;
                    case "D12":
                        result = delete(parameters);
                        break;
                    case "C13":
                        result = queryWeather(parameters);
                        break;
                }
            } else {
                result = "ERROR: Cantidad de parametros recibidos incorrectos\n";
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = "Error inesperado en el servidor socket.";
        }
        this.sendResultToClient(result);

    }

    private void sendResultToClient(String result) {
        try {
            OutputStream outputStream = service.getOutputStream();
            outputData = new DataOutputStream(outputStream);

            System.out.printf("RESULTADO => [ %s ]", result);

            outputData.writeUTF(result);
            outputData.flush();
            if (closeServer) {
                this.shutdownServer();
                this.serverListening = false;
            }
        } catch (IOException ex) {
            System.out.println("IOException while trying to send result to client => " + ex);
            ex.printStackTrace();
        }
    }

    private void shutdownServer() {
        try {
            if (outputData != null)
                outputData.close();
            if (inputData != null)
                inputData.close();
            if (socketService != null) {
                socketService.close();
                System.out.printf("#### El servidor en el puerto [%d] esta abajo.#####\n", this.getServerPort());
            }
            if (service != null)
                service.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    /**
     * This is a method to validate input parameters before request processing.
     *
     * @param request
     * @param parametersCounter
     * @return
     */
    private boolean checkRequest(String request, int parametersCounter) {

        System.out.printf("Se recibio la accion [%s] con [%d] parametros\n", request, parametersCounter);

        if (request.equals("A10") && parametersCounter == 11) {
            return true;
        } else if (request.equals("B11") && parametersCounter == 11) {
            return true;
        } else if (request.equals("D12") && parametersCounter == 4) {
            return true;
        } else return request.equals("C13") && (parametersCounter == 3 || parametersCounter == 4);
    }

    /**
     * 1.(9ptos) Recibir solicitudes para agregar (A)datos meteorológicosde una estaciónde la siguiente manera:
     * Si el socket cliente envíala cadena
     * 0        1               2           3       4                       5                   6                   7           8               9
     * “A10 | <id_estacion> | <fecha> | <hora> | <temperatura_maxima> | <temperatura_minima> | <velocidad_viento> | <presion> | <humedad> | <precipitacion_mm> | w"
     * se deberá ingresar el elemento en la colección y enviar un mensaje al Socket Cliente. El último dato corresponde al estado que siempre debe ser “w”
     * cuando se agrega un nuevo elemento. Se debe verificar que la temperatura máximasea mayor que la mínima, la fecha no puede ser mayor al año actual (2020/12/31)
     * y la hora no puede ser mayor a 23:59:59.
     * Estasvalidaciones se realizan del lado del servidory en caso de no cumplirse se debe retornar un mensajedescriptivo.
     *
     * @param parameters
     * @return
     */
    private String add(String[] parameters) {

        LocalDate date = TimeUtils.getDate(parameters[2]);

        //validate date. it shouldn't be greater than 2020
        if (date.isAfter(LocalDate.of(2020, 12, 31))) {
            return "ERROR: La fecha no puede ser mayor al 31/12/2020";
        }

        //validate temperature
        if (Integer.parseInt(parameters[4]) <= Integer.parseInt(parameters[5])) {
            return "ERROR: La temperatura maxima debe ser mayor a la minima.";
        }

        LocalTime time = null;
        try {
            time = TimeUtils.getTime(parameters[3]);
        } catch (DateTimeParseException e) {
            return "ERROR: Hora invalida.";
        }

        //id de la estación, la fecha(AAAAMMDD)y hora(HHMMSS)como clave
        String key = getWeatherIdFromParameters(parameters);

        WeatherData weatherData = WeatherData.buildWeatherDataFromParameters(key, parameters, time, date);

        weatherMap.put(key, weatherData);

        return String.format("Se agregaron los datos => %s", weatherData.toString());

    }

    /**
     * 2.(11ptos) Recibir solicitudes para modificar (B)datos meteorológicosde una estaciónde la siguiente manera:Si el servidor recibe
     * <p>
     * 0        1               2        3           4                   5                       6                       7           8           9                   10
     * “B11 | <id_estacion> | <fecha> | <hora> | <temperatura_maxima> | <temperatura_minima> | <velocidad_viento> | <presion> | <humedad> | <precipitacion_mm> | <estado>"
     * se debe validar si existe un elemento con esa clavey actualizar los datos.
     * Validaciones que se deben hacerdel lado del servidor antes de la actualización y en caso de no cumplirse se debe retornar un mensajedescriptivo:
     * •La nueva temperatura máxima debe ser mayor que la nueva temperatura mínima
     * •La precipitación debe ser mayor o igual a la precipitación almacenada.
     * •Los estados posibles son “w”ó“h”• Si la nueva presion es mayor quela presion almacenada, la nueva velocidad debe ser menor a la velocidad almacenada y viceversa.
     *
     * @param parameters
     * @return
     */
    private String update(String[] parameters) {

        String key = getWeatherIdFromParameters(parameters);

        if (weatherMap.containsKey(key)) {

            WeatherData weatherData = (WeatherData) weatherMap.get(key);

            //first, let's validate allll the parameters
            //validate temperature
            if (Integer.parseInt(parameters[4]) <= Integer.parseInt(parameters[5])) {
                return "ERROR: La temperatura maxima debe ser mayor a la minima.";
            }

            //validate precipitation
            if (Integer.parseInt(parameters[9]) < weatherData.getPrecipitacion()) {
                return "ERROR: La precipitación debe ser mayor o igual a la precipitación almacenada.";
            }

            //status validation
            if (!parameters[10].equalsIgnoreCase("w") || !parameters[10].equalsIgnoreCase("h")) {
                return "ERROR: Los estados posibles son [w] ó [h]";
            }

            //pressure and velocity validation
//            Si la nueva presion es mayor que la presion almacenada, la nueva velocidad debe ser menor a la velocidad almacenada y viceversa.
            if (Integer.parseInt(parameters[7]) > weatherData.getPresion() && Integer.parseInt(parameters[6]) > weatherData.getVelocidadViento()) {
                return "ERROR: Si la nueva presion es mayor que la presion almacenada, la nueva velocidad debe ser menor a la velocidad almacenada.";
            } else if (Integer.parseInt(parameters[7]) < weatherData.getPresion() && Integer.parseInt(parameters[6]) < weatherData.getVelocidadViento()) {
                return "ERROR: Si la nueva presion es menor que la presion almacenada, la nueva velocidad debe ser mayor a la velocidad almacenada.";
            }

            LocalDate newDate = TimeUtils.getDate(parameters[2]);
            //validate date. it shouldn't be greater than 2020
            if (newDate.isAfter(LocalDate.of(2020, 12, 31))) {
                return "ERROR: La fecha no puede ser mayor al 31/12/2020";
            }

            //validate temperature
            if (Integer.parseInt(parameters[4]) <= Integer.parseInt(parameters[5])) {
                return "ERROR: La temperatura maxima debe ser mayor a la minima.";
            }

            LocalTime newTime = null;
            try {
                newTime = TimeUtils.getTime(parameters[3]);
            } catch (DateTimeParseException e) {
                return "ERROR: Hora invalida.";
            }

            //now, we can finally update the map.
            WeatherData newWeather = WeatherData.buildWeatherDataFromParameters(key, parameters, newTime, newDate);

            weatherMap.put(key, newWeather);

            return String.format("Se modificaron los datos meteorologicos de la clave %s a %s", key, newWeather);

        } else {
            return String.format("No existe ningun dato meteorologico con la clave %s a ser modificado.", key);
        }
    }

    /**
     * (7ptos)
     * Recibir solicitudes para eliminar(D) datos meteorológicosde una estaciónde la siguiente manera:Si el servidor recibe
     * “D12|<id estacion> |<fecha> |<hora>” se debe validar si existe un elemento con esa clavecon estado diferente a H
     * y retornar el contenido del elemento eliminado o un mensaje indicando que no existe elementos que coincida con los datos ingresadosal Socket Cliente.
     * (se debe utilizar Java Streamstanto en el filtro como en la eliminación“removeIf”)
     *
     * @param parameters
     * @return
     */
    private String delete(String[] parameters) {

        String key = getWeatherIdFromParameters(parameters);

        if (weatherMap.containsKey(key) && weatherMap.values().stream().noneMatch(weatherData -> weatherData.getStatus().equals("h"))) {

            WeatherData removed = weatherMap.entrySet().stream()
                    .filter(weather -> weather.getKey().equals(key))
                    .findFirst()
                    .get()
                    .getValue();

            if (weatherMap.keySet().removeIf(weatherKey -> weatherKey.equals(key))) {
                return String.format("Se ha eliminado el elemento %s", removed);
            } else {
                return "No se ha podido eliminar ningun elemento.";
            }
        } else {
            return String.format("No existe ningun dato meteorologico con la clave %s o el estado H", key);
        }

    }

    /**
     * C13 | 01 | <id_estacion> | <año>
     * C13 | 11 | <año>
     *
     * @param parameters
     * @return
     */
    private String queryWeather(String[] parameters) {
        if (parameters[1].equals("01") && parameters.length == 4) {
            return queryBySeasonAndYear(parameters);
        } else if (parameters[1].equals("11") && parameters.length == 3) {
            return queryByYear(parameters);
        } else {
            return "Parametros de consulta invalidos.";
        }
    }

    /**
     * el servidor recibe“C13|01| <id_estacion> | <año>” se debe validar y buscar todos los datos de
     * la estación y año recibido(con estado igual a H) y retornar el siguiente resumen:
     * <id_estacion> ,<año>->
     * <promedio_temperatura>-<maxima_temperatura>-
     * <minima_temperatura>-<mayor_velocidad_viento>-
     * <menor_velocidad_viento>-<promedio_presion>-
     * <promedio_humedad> -<promedio_precipitacion_mm>.
     * Los datos están agrupados por estación y año(se debe utilizar Java Streams).
     *
     * @param parameters
     * @return
     */
    private String queryBySeasonAndYear(String[] parameters) {

        String seasonId = parameters[2];

        int year = Integer.parseInt(parameters[3]);

        StringBuilder builder = new StringBuilder();

        if (weatherMap.values().stream().anyMatch(weatherData -> weatherData.getIdEstacion().equals(seasonId) && weatherData.getAnho() == year)) {

            Map<String, Map<Integer, List<WeatherData>>> collect = weatherMap.values().stream()
                    .filter(weatherData -> weatherData.getIdEstacion().equals(seasonId) && weatherData.getAnho() == year)
                    .collect(Collectors.groupingBy(WeatherData::getIdEstacion, Collectors.groupingBy(WeatherData::getAnho)));

            collect.forEach(
                    (key, value) -> value.forEach(((integer, weatherData) -> {
                        Double temperatureAverage = weatherData.stream().collect(Collectors.averagingInt(WeatherData::getTemperaturaMaxima));
                        int maxTemperature = weatherData.stream().max(Comparator.comparing(WeatherData::getTemperaturaMaxima)).get().getTemperaturaMaxima();
                        int minTemperature = weatherData.stream().min(Comparator.comparing(WeatherData::getTemperaturaMinima)).get().getTemperaturaMinima();
                        Double majorWindVel = weatherData.stream().max(Comparator.comparing(WeatherData::getVelocidadViento)).get().getVelocidadViento();
                        Double minorWindVel = weatherData.stream().min(Comparator.comparing(WeatherData::getVelocidadViento)).get().getVelocidadViento();
                        Double pressureAvg = weatherData.stream().collect(Collectors.averagingInt(WeatherData::getPresion));
                        Double humidityAvg = weatherData.stream().collect(Collectors.averagingInt(WeatherData::getHumedad));
                        Double precipitationAvg = weatherData.stream().collect(Collectors.averagingInt(WeatherData::getPrecipitacion));
                        builder.append(String.format("<%s, %d> <%f> - <%d> - <%d> - <%f> - <%f> - <%f> - <%f> - <%f>\n", key, integer, temperatureAverage, maxTemperature, minTemperature, majorWindVel, minorWindVel, pressureAvg, humidityAvg, precipitationAvg));
                    }))
            );
            return builder.toString();
        } else {
            return String.format("No existen datos para la estacion %s y el anho %d", seasonId, year);
        }

    }

    /**
     * Si el servidor recibe “C13 | 11 | <año>” se debe validar y buscar todos los datos del año recibido y retornar el siguiente resumen:
     * <año> -> <promedio_ velocidad_viento> - <mayor_ presion > - <menor_ presion> - <mayor_ precipitacion_mm> - <menor_ precipitacion_mm>.
     * Los datos están agrupados por año (se debe utilizar Java Streams).
     *
     * @param parameters
     * @return
     */
    private String queryByYear(String[] parameters) {

        int year = Integer.parseInt(parameters[2]);

        if (weatherMap.values().stream().anyMatch(weatherData -> weatherData.getAnho() == year)) {

            StringBuilder builder = new StringBuilder();

            Map<Integer, List<WeatherData>> queryResult = weatherMap.values().stream()
                    .filter(weatherData -> weatherData.getAnho() == year)
                    .collect(Collectors.groupingBy(WeatherData::getAnho));

            queryResult.forEach(
                    (anho, weather) -> {
                        Double windAvg = weather.stream().collect(Collectors.averagingDouble(WeatherData::getVelocidadViento));
                        int majorPress = weather.stream().max(Comparator.comparing(WeatherData::getPresion)).get().getPresion();
                        int minorPress = weather.stream().min(Comparator.comparing(WeatherData::getPresion)).get().getPresion();
                        int majorPrec = weather.stream().max(Comparator.comparing(WeatherData::getPrecipitacion)).get().getPrecipitacion();
                        int minorPrec = weather.stream().min(Comparator.comparing(WeatherData::getPrecipitacion)).get().getPrecipitacion();
                        builder.append(String.format("<%d> <%f> - <%d> - <%d> - <%d> - <%d>\n", anho, windAvg, majorPress, minorPress, majorPrec, minorPrec));
                    }
            );

            return builder.toString();

        } else {
            return String.format("No existen datos para el anho %s", year);
        }
    }

    private String getWeatherIdFromParameters(String[] parameters) {
        return parameters[1].concat(parameters[2]).concat(parameters[3]);
    }
}
