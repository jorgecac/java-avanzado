package segundo.parcial.beans;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by jcaceresf on 10/14/20
 */
public class WeatherData {

    private String key;
    private String idEstacion;
    private int anho;
    private LocalDate fecha;
    private LocalTime hora;
    private int temperaturaMaxima;
    private int temperaturaMinima;
    private double velocidadViento;
    private int presion;
    private int humedad;
    private int precipitacion;
    private String status;

    public static WeatherData buildWeatherDataFromParameters(String key, String[] parameters, LocalTime time, LocalDate date) {
        WeatherData weatherData = new WeatherData();
        weatherData.setKey(key);
        weatherData.setIdEstacion(parameters[1]);
        weatherData.setAnho(date.getYear());
        weatherData.setFecha(date);
        weatherData.setHora(time);
        weatherData.setTemperaturaMaxima(Integer.parseInt(parameters[4]));
        weatherData.setTemperaturaMinima(Integer.parseInt(parameters[5]));
        weatherData.setVelocidadViento(Integer.parseInt(parameters[6]));
        weatherData.setPresion(Integer.parseInt(parameters[7]));
        weatherData.setHumedad(Integer.parseInt(parameters[8]));
        weatherData.setPrecipitacion(Integer.parseInt(parameters[9]));
        weatherData.setStatus(parameters[10]);
        return weatherData;
    }


    public String getIdEstacion() {
        return idEstacion;
    }

    public void setIdEstacion(String idEstacion) {
        this.idEstacion = idEstacion;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public LocalTime getHora() {
        return hora;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

    public int getTemperaturaMaxima() {
        return temperaturaMaxima;
    }

    public void setTemperaturaMaxima(int temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
    }

    public int getTemperaturaMinima() {
        return temperaturaMinima;
    }

    public void setTemperaturaMinima(int temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    public double getVelocidadViento() {
        return velocidadViento;
    }

    public void setVelocidadViento(double velocidadViento) {
        this.velocidadViento = velocidadViento;
    }

    public int getPresion() {
        return presion;
    }

    public void setPresion(int presion) {
        this.presion = presion;
    }

    public int getHumedad() {
        return humedad;
    }

    public void setHumedad(int humedad) {
        this.humedad = humedad;
    }

    public int getPrecipitacion() {
        return precipitacion;
    }

    public void setPrecipitacion(int precipitacion) {
        this.precipitacion = precipitacion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getAnho() {
        return anho;
    }

    public void setAnho(int anho) {
        this.anho = anho;
    }

    @Override
    public String toString() {
        return "Clave='" + key + '\'' +
                ", ID. Estacion ='" + idEstacion + '\'' +
                ", Fecha =" + fecha +
                ", Hora =" + hora +
                ", Temp. Maxima =" + temperaturaMaxima +
                ", Temp. Minima =" + temperaturaMinima +
                ", Veloc. Vient =" + velocidadViento +
                ", Presion =" + presion +
                ", Humedad =" + humedad +
                ", Precipitacion =" + precipitacion +
                ", Estado ='" + status;
    }
}
