package segundo.parcial.utils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Created by jcaceresf on 10/14/20
 */
public class TimeUtils {

    private static final String DATE_PATTERN = "yyyyMMdd";
    private static final String TIME_PATTERN = "HHmmss";

    public static LocalDate getDate(String dateStr) {
        return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(DATE_PATTERN));
    }

    public static LocalTime getTime(String timeStr) throws DateTimeParseException {
        return LocalTime.parse(timeStr, DateTimeFormatter.ofPattern(TIME_PATTERN));
    }
}
