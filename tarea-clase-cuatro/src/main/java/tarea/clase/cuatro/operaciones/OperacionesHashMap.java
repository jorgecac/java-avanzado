package tarea.clase.cuatro.operaciones;

import tarea.clase.cuatro.beans.Producto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;


public class OperacionesHashMap {
    private HashMap<Integer, Producto> mapaProductos = new HashMap<Integer, Producto>();
    private Queue<Producto> colaProductos = new LinkedBlockingQueue<Producto>();
    private BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));

    public void Menu() throws IOException {

        int opcion = 0;

        while (opcion != 7) {

            System.out.println("Selecione una opcion");
            System.out.println("1- Agregar Producto");
            System.out.println("2- Actualizar Producto segun codigo de producto");
            System.out.println("3- Eliminar Producto segun codigo de producto");
            System.out.println("4- Listar Producto");
            System.out.println("5- Listar Producto ordenado por precio");
            System.out.println("6- Vaciar productos");
            System.out.println("7- Salir");

            System.out.println("Ingrese una opcion");

            opcion = Integer.parseInt(entrada.readLine());

            switch (opcion) {
                case 1:
                    agregarProducto();
                    break;
                case 2:
                    actualizarProducto();
                    break;
                case 3:
                    eliminarProducto();
                    break;
                case 4:
                    imprimirListaProducto();
                    break;
                case 5:
                    imprimirListaProductoOrdenado();
                    break;
                case 6:
                    vaciar();
                    break;
                case 7:
                    System.out.println("Hasta luego");
                    break;
                default:
                    System.out.println("Opcion incorrecta");
            }

        }

    }

    private void agregarProducto() throws IOException {
        Producto productoNuevo = new Producto();

        System.out.println("Ingrese codigo");
        productoNuevo.setCodigo(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese nombre");
        productoNuevo.setNombre(entrada.readLine());

        System.out.println("Ingrese procedencia");
        productoNuevo.setProcedencia(entrada.readLine());

        System.out.println("Ingrese la cantidad en Stock");
        productoNuevo.setCantidadStock(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese el precio");
        productoNuevo.setPrecio(Integer.parseInt(entrada.readLine()));

        System.out.println("Ingrese el estado (A=activo, I=inactivo)");
        productoNuevo.setEstado(entrada.readLine().charAt(0));

        if (mapaProductos.containsKey(productoNuevo.getCodigo())) {
            System.out.println("Producto existente, desea sobreescribir los datos (SI/NO)");
            String respuesta = entrada.readLine();
            if (respuesta.equalsIgnoreCase("NO")) {
                return;
            }
        }
        mapaProductos.put(productoNuevo.getCodigo(), productoNuevo);
        System.out.println("Producto agregado");

    }

    private void actualizarProducto() throws IOException {
        if (mapaProductos.isEmpty()) {
            System.out.println("El mapa no contiene productos");
            return;
        }
        Integer codProducto;
        System.out.println("Ingrese el codigo del producto a actualizar");
        codProducto = Integer.parseInt(entrada.readLine());

        if (mapaProductos.containsKey(codProducto)) {
            Producto productoaActualizar = mapaProductos.get(codProducto);
            System.out.println("Producto a actualizar");
            System.out.println(productoaActualizar.toString());

            System.out.println("Ingrese la cantidad en Stock a actualizar");
            productoaActualizar.setCantidadStock(Integer.parseInt(entrada.readLine()));

            System.out.println("Ingrese el precio a actualizar");
            productoaActualizar.setPrecio(Integer.parseInt(entrada.readLine()));

            System.out.println("Ingrese el estado a actualizar (A=activo, I=inactivo)");
            productoaActualizar.setEstado(entrada.readLine().charAt(0));

            mapaProductos.put(codProducto, productoaActualizar);
            System.out.println("Producto actualizado");

        } else {
            System.out.println("El codigo del producto ingreso no existe");
        }
    }

    private void eliminarProducto() throws IOException {
        if (mapaProductos.isEmpty()) {
            System.out.println("El mapa no contiene productos");
            return;
        }
        Integer codProducto;
        System.out.println("Ingrese el codigo del producto a eliminar");
        codProducto = Integer.parseInt(entrada.readLine());

        if (mapaProductos.containsKey(codProducto)) {
            Producto productoEliminado = mapaProductos.remove(codProducto);
            System.out.println("Producto eliminado");
            System.out.println(productoEliminado.toString());
        } else {
            System.out.println("El codigo del producto ingresado no existe");
        }
    }

    private void imprimirListaProducto() {
        if (mapaProductos.isEmpty()) {
            System.out.println("El mapa no contiene productos");
            return;
        }

        mapaProductos.forEach((key, value) -> {
            System.out.println("Clave producto: [" + key + "] , Producto: " + value.toString());
        });

        System.out.println("Cantidad de productos: " + mapaProductos.size());
    }

    private void imprimirListaProductoOrdenado() {
        if (mapaProductos.isEmpty()) {
            System.out.println("El mapa no contiene productos");
            return;
        }

        mapaProductos.forEach((key, value) -> colaProductos.offer(value));

        List<Producto> listaOrdenada = colaProductos.stream()
                .sorted(Producto::compareTo)
                .collect(Collectors.toList());

        System.out.println("Cantidad de productos: " + listaOrdenada.size());

        System.out.println("Lista de productos ordenada:");

        listaOrdenada.forEach(System.out::println);

    }

    private void vaciar() {
        if (mapaProductos.isEmpty()) {
            System.out.println("La lista no contiene ningun producto.");
            return;
        } else {
            mapaProductos.clear();
            if (!colaProductos.isEmpty()) {
                colaProductos.clear();
            }
        }
    }

}
