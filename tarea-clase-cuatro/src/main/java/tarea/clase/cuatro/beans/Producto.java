package tarea.clase.cuatro.beans;

import java.util.Objects;

public class Producto implements Comparable<Producto> {
    private Integer codigo;
    private String nombre;
    private String procedencia;
    private Integer cantidadStock;
    private int precio;
    private Character estado;

    public Producto() {
    }

    public Producto(Integer codigo, String nombre, String procedencia, Integer cantidadStock, int precio, Character estado) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.procedencia = procedencia;
        this.cantidadStock = cantidadStock;
        this.precio = precio;
        this.estado = estado;
    }


    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public Integer getCantidadStock() {
        return cantidadStock;
    }

    public void setCantidadStock(Integer cantidadStock) {
        this.cantidadStock = cantidadStock;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", procedencia='" + procedencia + '\'' +
                ", cantidadStock=" + cantidadStock +
                ", precio=" + precio +
                ", estado=" + estado +
                '}';
    }

    @Override
    public int compareTo(Producto o) {
        if (this.precio > o.getPrecio())
            return -1;

        if (this.precio < o.getPrecio())
            return 1;

        return 0;
    }

}
