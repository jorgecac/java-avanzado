package primer.parcial.operations;

import primer.parcial.beans.Cancion;
import primer.parcial.utils.EntradaUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by jcaceresf on 9/9/20
 */
public class Operation {


    private Set<Cancion> canciones = new HashSet<>();
    private BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));

    public void Menu() {

        try {
            int opcion = 0;

            while (opcion != 7) {

                System.out.println("Selecione una opcion");
                System.out.println("1- Agregar Cancion");
                System.out.println("2- Imprimir todas las canciones activas");
                System.out.println("3- Eliminar una cancion. Dado un titulo y un anho.");
                System.out.println("4- Actualizar el estado, la marca de hit y la duración de una canción");
                System.out.println("5- Imprimir todas las canciones de un año.");
                System.out.println("6- Imprimir todas las canciones de un artista.");
                System.out.println("7- Salir");

                System.out.println("Ingrese una opcion");

                opcion = Integer.parseInt(entrada.readLine());

                switch (opcion) {
                    case 1:
                        agregarCancion();
                        break;
                    case 2:
                        imprimirCancionesActivas();
                        break;
                    case 3:
                        eliminarCancion();
                        break;
                    case 4:
                        actualizarCancion();
                        break;
                    case 5:
                        imprimirCancionesPorAnho();
                        break;
                    case 6:
                        imprimirCancionesPorArtista();
                        break;
                    case 7:
                        System.out.println("¡Hasta luego!");
                        break;
                    default:
                        System.out.println("Opcion incorrecta");
                }

            }
        } catch (IOException e) {
            System.out.println("Ha ocurrido un error en la entrada de datos.");
        }

    }

    private void agregarCancion() throws IOException {

        Cancion cancion = new Cancion();

        System.out.print("Ingrese titulo: ");
        cancion.setTitulo(entrada.readLine());

        cancion.setAnho(EntradaUtil.obtenerAnho(entrada));

        if (canciones.stream().anyMatch(c -> c.equals(cancion))) {
            System.out.println("Cancion existente, ingrese una nueva cancion.");
            return;
        }

        System.out.print("Ingrese el nombre del artista: ");
        cancion.setNombreArtista(entrada.readLine());

        System.out.print("Ingrese el album: ");
        cancion.setAlbum(entrada.readLine());

        cancion.setDuracion(EntradaUtil.obtenerDuracion(entrada));
        cancion.setGenero(EntradaUtil.obtenerGenero(entrada));
        cancion.setEstado(EntradaUtil.obtenerEstado(entrada));
        cancion.setHit(EntradaUtil.obtenerHit(entrada));
        canciones.add(cancion);
        System.out.printf("%s de %s ha sido agregada\n", cancion.getTitulo(), cancion.getNombreArtista());
    }

    private void imprimirCancionesActivas() {

        if (canciones.isEmpty()) {
            System.out.println("No existen canciones registradas.");
            return;
        }

        Set<Cancion> tmp = new HashSet<>(canciones);

        List<Cancion> a = tmp.stream()
                .filter(cancion -> cancion.getEstado().equals('A'))
                .collect(Collectors.toList());

        if (a.isEmpty()) {
            System.out.println("No existen canciones activas");
        } else {
            imprimirLista(a);
        }
    }

    /**
     * e. Imprimir todas las canciones de un año. (4 ptos)
     * Se debe cargar el año y mostrar todas las canciones que pertenecen al año ingresado.
     * No se debe tener en cuenta las canciones inactivas.
     */
    private void imprimirCancionesPorAnho() throws IOException {


        if (canciones.isEmpty()) {
            System.out.println("No existen canciones registradas.");
            return;
        }
        int anho = EntradaUtil.obtenerAnho(entrada);

        Set<Cancion> tmp = new HashSet<>(canciones);

        List<Cancion> a = tmp.stream()
                .filter(cancion -> cancion.getEstado().equals('A') && cancion.getAnho() == anho)
                .collect(Collectors.toList());

        if (a.isEmpty()) {
            System.out.printf("No existen canciones para el anho %d", anho);
        } else {
            imprimirLista(a);
        }
    }

    /**
     * f. Imprimir todas las canciones de un artista. (5 ptos)
     * Se debe cargar el nombre del artista y mostrar todas las canciones
     * que fueron compuestas por el artista ingresado. Deben estar ordenas por año, de mayor a menor.
     */
    private void imprimirCancionesPorArtista() throws IOException {
        if (canciones.isEmpty()) {
            System.out.println("No existen canciones registradas.");
            return;
        }

        System.out.print("Ingrese el nombre del artista:");
        String artista = entrada.readLine();

        Set<Cancion> tmp = new HashSet<>(canciones);

        List<Cancion> a = tmp.stream()
                .filter(cancion -> cancion.getNombreArtista().equals(artista))
                .sorted(Cancion::compareTo)
                .collect(Collectors.toList());
        if (a.isEmpty()) {
            System.out.printf("No existen canciones para el artista %s", artista);
        } else {
            imprimirLista(a);
        }
    }


    /**
     * Eliminar una canción. (9 ptos)
     * Se debe solicitar por teclado el título y año de una canción. Validar que exista la canción y eliminarla mostrando un mensaje de confirmación.
     * En caso de que la canción no exista mostrar un mensaje de alerta indicando que la canción no existe.
     */
    private void eliminarCancion() throws IOException {


        if (canciones.isEmpty()) {

            System.out.println("No existen canciones registradas.");

        } else {

            System.out.print("Ingrese el titulo de la cancion");
            String titulo = entrada.readLine();
            int anho = EntradaUtil.obtenerAnho(entrada);

            if (canciones.stream().anyMatch(cancion -> cancion.getAnho() == anho && cancion.getTitulo().equals(titulo))) {
                boolean remove = canciones.removeIf(cancion -> cancion.getAnho() == anho && cancion.getTitulo().equals(titulo));
                if (remove) {
                    System.out.println("La cancion ha sido eliminada.");
                }
            } else {
                System.out.println("No se encuentra la cancion dentro de la lista.");
            }
        }


    }


    /**
     * Actualizar el estado, la marca de hit y la duración de una canción. (15 ptos)
     * Se debe solicitar por teclado el título y año de una canción. Validar que exista la canción y
     * posteriormente solicitar los datos que van a ser actualizados. Realizar las siguientes validaciones:
     * i. La nueva duración de la canción no puede ser mayor a 7 minutos.
     * ii. Si el nuevo estado de la canción es “I” la nueva marca de hit debe cambiarse a “N”.
     * iii. La nueva marca de hit solo puede cambiar a “S” si el nuevo estado de la canción será “A”.
     */
    private void actualizarCancion() throws IOException {

        if (canciones.isEmpty()) {
            System.out.println("No existen canciones registradas.");
        } else {

            System.out.print("Ingrese el titulo de la cancion");
            String titulo = entrada.readLine();

            int anho = EntradaUtil.obtenerAnho(entrada);

            if (canciones.stream().anyMatch(cancion -> cancion.getAnho() == anho && cancion.getTitulo().equals(titulo))) {
                for (Cancion cancion : canciones) {
                    if (cancion.getAnho() == anho && cancion.getTitulo().equals(titulo)) {
                        cancion.setDuracion(EntradaUtil.obtenerDuracion(entrada));
                        Character estado = EntradaUtil.obtenerEstado(entrada);
                        cancion.setEstado(estado);
                        if (estado.equals('I')) {
                            cancion.setHit('N');
                        } else if (estado.equals('A')) {
                            cancion.setHit('S');
                        }
                    }
                }
            }
        }
    }

    private <T> void imprimirLista(List<T> elementos) {

        elementos.forEach(elemento -> System.out.print(elemento.toString()));

    }


}
