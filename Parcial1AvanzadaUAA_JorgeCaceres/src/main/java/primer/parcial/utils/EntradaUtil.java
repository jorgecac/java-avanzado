package primer.parcial.utils;

import primer.parcial.beans.Cancion;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by jcaceresf on 9/9/20
 */
public class EntradaUtil {

    public static String obtenerGenero(BufferedReader entrada) throws IOException {

        String generoStr = "";

        int genero = 0;

        while (genero > 5 || genero < 1) {
            System.out.println("Ingrese el numero correspondiente al genero de la cancion: ");
            System.out.println("1 - Bachata");
            System.out.println("2 - Balada");
            System.out.println("3 - Clasica");
            System.out.println("4 - Electronica");
            System.out.println("5 - Pop");
            genero = Integer.parseInt(entrada.readLine());
        }

        switch (genero) {
            case 1:
                generoStr = "Bachata";
                break;
            case 2:
                generoStr = "Balada";
                break;
            case 3:
                generoStr = "Bachata";
                break;
            case 4:
                generoStr = "Elecronica";
                break;
            case 5:
                generoStr = "Pop";
                break;
        }
        return generoStr;
    }

    public static Character obtenerHit(BufferedReader entrada) throws IOException {

        Character character = 'X';

        while (!character.equals('S') && !character.equals('N')) {
            System.out.print("Ingrese S si la cancion es un hit o N si no lo es: ");
            character = entrada.readLine().charAt(0);
        }
        return character;
    }

    public static Character obtenerEstado(BufferedReader entrada) throws IOException {

        Character character = 'X';
        while (!character.equals('A') && !character.equals('I')) {
            System.out.print("Ingrese A para ACTIVA o I para INACTIVA: ");
            character = entrada.readLine().charAt(0);
        }
        return character;
    }

    public static int obtenerDuracion(BufferedReader entrada) throws IOException {

        int duracion = 1000;

        while (duracion > 420) {

            System.out.print("Ingrese la duracion de la cancion en segundos. No puede ser mayor a 7 minutos (420 segundos): ");
            try {
                duracion = Integer.parseInt(entrada.readLine());
            } catch (NumberFormatException e) {
                System.out.println("Por favor, ingrese una duracion en numeros enteros validos.");
            }
        }
        return duracion;
    }

    public static int obtenerAnho(BufferedReader entrada) throws IOException {
        System.out.print("Ingrese el anho de la cancion: ");

        int anho = 0;
        while (anho < 1) {
            try {
                anho = Integer.parseInt(entrada.readLine());
            } catch (NumberFormatException e) {
                System.out.println("Por favor, ingrese un anho entero y validos");
            }
        }
        return anho;
    }

}
