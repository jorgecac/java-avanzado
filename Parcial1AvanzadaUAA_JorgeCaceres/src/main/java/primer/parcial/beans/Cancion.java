package primer.parcial.beans;

import java.util.Objects;

/**
 * Created by jcaceresf on 9/9/20
 */
public class Cancion implements Comparable {

    private String titulo;
    private String nombreArtista;
    private String album;
    private String genero;
    private int anho;
    private int duracion;
    private Character estado;
    private Character hit;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getNombreArtista() {
        return nombreArtista;
    }

    public void setNombreArtista(String nombreArtista) {
        this.nombreArtista = nombreArtista;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getAnho() {
        return anho;
    }

    public void setAnho(int anho) {
        this.anho = anho;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getHit() {
        return hit;
    }

    public void setHit(Character hit) {
        this.hit = hit;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Cancion other = (Cancion) obj;
        return Objects.equals(this.titulo, other.titulo) && Objects.equals(this.anho, other.anho);
    }

    @Override
    public int compareTo(Object o) {

        Cancion cancion = (Cancion) o;

        return Integer.compare(this.anho, cancion.getAnho());

    }

    /**
     * (A=activa, I=inactiva)
     *
     * @return
     */
    public String getEstadoLegible() {
        return this.estado.equals('A') ? "ACTIVA" : "INACTIVA";
    }

    /**
     * (S=si, N=no)
     *
     * @return
     */
    public String getHitLegible() {
        return this.hit.equals('S') ? "SI" : "NO";
    }

    @Override
    public String toString() {
        return String.format("%s de %s del album %s - %d - %s - %d seg. - %s - ¿Es un HIT? %s \n",
                this.titulo, this.nombreArtista, this.album, this.anho, this.genero, this.duracion, this.getEstadoLegible(), this.getHitLegible());
    }
}
