package tarea.clase.cinco;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jcaceresf on 9/2/20
 */
public class OperacionGeneric<E> {

    /**
     * Método que calcule el menor elemento de un arreglo de cualquier tipo de dato
     *
     * @param arreglo arreglo de cualquier tipo.
     * @param <E>
     * @return
     */
    public <E extends Comparable<E>> void obtenerMinimo(E[] arreglo) {

        E minimo = arreglo[0];

        for (E elemento : arreglo) {
            if (elemento.compareTo(minimo) < 0) {
                minimo = elemento;
            }
        }
        System.out.printf("El elemento minimo del arreglo es -> %s\n", minimo);
    }

    /**
     * Método que calcule el mayor elemento del arreglo de cualquier tipo de dato
     *
     * @param arreglo arreglo de cualquier tipo
     * @param <E>
     * @return
     */
    public <E extends Comparable<E>> void obtenerMaximo(E[] arreglo) {

        E maximo = arreglo[0];

        for (E elemento : arreglo) {
            if (elemento.compareTo(maximo) > 0) {
                maximo = elemento;
            }
        }
        System.out.printf("El elemento maximo del arreglo es -> %s \n", maximo);
    }

    /**
     * Método que imprima los elementos del arreglo de cualquier tipo de dato de manera ordenada
     */
    public void imprimir(E[] arreglo) {
        List<E> lista = Arrays.stream(arreglo).sorted().collect(Collectors.toList());
        lista.forEach(elemento -> System.out.printf("Elemento [%s]\n", elemento));
    }


}
