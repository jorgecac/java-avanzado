package tarea.clase.cinco;

import java.util.Random;

/**
 * Created by jcaceresf on 9/2/20
 */
public class Main {

    public static void main(String[] args) {

        Random random = new Random();

        OperacionGeneric<Integer> operacionInteger = new OperacionGeneric<Integer>();
        Integer integers[] = new Integer[]{random.nextInt(), random.nextInt(), random.nextInt(), random.nextInt(), random.nextInt(), random.nextInt()};
        operacionInteger.obtenerMaximo(integers);
        operacionInteger.obtenerMinimo(integers);
        operacionInteger.imprimir(integers);


        OperacionGeneric<String> operacionString = new OperacionGeneric<>();
        String strings[] = new String[]{"A", "Z", "F", "Y", "U"};
        operacionString.obtenerMaximo(strings);
        operacionString.obtenerMinimo(strings);
        operacionString.imprimir(strings);

        OperacionGeneric<Long> operacionLong = new OperacionGeneric<>();
        Long longs[] = new Long[]{random.nextLong(), random.nextLong(), random.nextLong(), random.nextLong(), random.nextLong(), random.nextLong()};
        operacionLong.obtenerMaximo(longs);
        operacionLong.obtenerMinimo(longs);
        operacionLong.imprimir(longs);
    }

}
