CREATE TABLE public.pedidos
(
    fecha_solicitud date NOT NULL,
    cliente_id integer NOT NULL,
    nombre_consola character varying(250)[] COLLATE pg_catalog."default" NOT NULL,
    cantidad integer NOT NULL,
    lector_disco character varying(1)[] COLLATE pg_catalog."default" NOT NULL,
    cap_almacenamiento integer NOT NULL,
    can_procesadores integer NOT NULL,
    estado character varying(1)[] COLLATE pg_catalog."default" NOT NULL,
    fecha_entrega date,
    fecha_pago date,
    CONSTRAINT pk_pedidos PRIMARY KEY (fecha_solicitud, cliente_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.pedidos
    OWNER to postgres;