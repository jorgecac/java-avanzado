package py.com.jacaceresf.tercerparcialrest.services;

import py.com.jacaceresf.tercerparcialrest.beans.Pedido;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by jcaceresf on 11/13/20
 */
public interface PedidoService {

    Pedido guardarPedido(Pedido pedido) throws Exception;

    List<Pedido> obtenerPendientes() throws Exception;

    List<Pedido> obtenerPedidosCliente(int idCliente) throws Exception;

    void eliminarPedidoCliente(int idCliente, LocalDate fechaPedido) throws Exception;

    void actualizarPedido(Pedido pedido) throws Exception;


}
