package py.com.jacaceresf.tercerparcialrest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import py.com.jacaceresf.tercerparcialrest.beans.Pedido;
import py.com.jacaceresf.tercerparcialrest.dao.PedidoDAO;
import py.com.jacaceresf.tercerparcialrest.dao.PedidoDAOImpl;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jcaceresf on 11/13/20
 */
@Service
public class PedidoServiceImpl implements PedidoService {

    @Autowired
    private PedidoDAO pedidoDAO;

    @Autowired(required = false)
    public PedidoServiceImpl(DataSource dataSource) {
        pedidoDAO = new PedidoDAOImpl(dataSource);
    }

    public PedidoServiceImpl() {
    }

    @Override
    public Pedido guardarPedido(Pedido pedido) throws Exception {
        if (!pedidoDAO.verificarExistencia(pedido.getIdCliente(), pedido.getFechaSolicitud(), pedido.getNombreConsola())) {

            if (pedido.getLector() != 'S' && pedido.getLector() != 'N') {
                throw new Exception("El lector solo puede recibir el valor de N o S.");
            }
            if (pedido.getEstado() != 'S') {
                throw new Exception("El estado inicial del pedido no puede ser distinto a S.");
            }
            return pedidoDAO.guardarPedido(pedido);

        } else {
            throw new Exception("Ya existe un pedido para el cliente, la fecha de solicitud y el nombre de consola especificada.");
        }
    }

    @Override
    public List<Pedido> obtenerPendientes() throws Exception {

        List<Pedido> pedidos = pedidoDAO.obtenerPendientes();

        //we can do this at the SQL, but let's sorted it with stream.
        return pedidos.stream()
                .sorted(Comparator.comparing(Pedido::getFechaEstimadaEntrega, Comparator.nullsLast(Comparator.reverseOrder()))).collect(Collectors.toList());
    }

    @Override
    public List<Pedido> obtenerPedidosCliente(int idCliente) throws Exception {
        return pedidoDAO.obtenerPedidosClient(idCliente);
    }

    @Override
    public void eliminarPedidoCliente(int idCliente, LocalDate fechaPedido) throws Exception {

        if (idCliente <= 0) {
            throw new Exception("Parametro incorrecto para idCliente.");
        }

        if (fechaPedido == null) {
            throw new Exception("Parametro incorrecto para fecha pedido.");
        }

        pedidoDAO.eliminarPedidoClient(idCliente, fechaPedido);
    }

    @Override
    public void actualizarPedido(Pedido pedido) throws Exception {

        Pedido pedidoExistente = pedidoDAO.obtenerPedido(pedido.getIdCliente(), pedido.getFechaSolicitud(), pedido.getNombreConsola());

        verificarParametrosActualizacion(pedidoExistente, pedido);

        pedidoDAO.actualizarPedido(pedido);
    }

    private void verificarParametrosActualizacion(Pedido pedidoDb, Pedido pedidoActualizar) throws Exception {

        if (StringUtils.hasText(pedidoActualizar.getNombreConsola()) && !pedidoActualizar.getNombreConsola().equals(pedidoDb.getNombreConsola())) {
            throw new Exception("No puede actualizar el nombre de la consola del pedido.");
        }

        if (pedidoActualizar.getCantidad() != pedidoDb.getCantidad()) {
            throw new Exception("No puede actualizar la cantidad del pedido.");
        }

        if (pedidoActualizar.getLector() != pedidoDb.getLector()) {
            throw new Exception("No puede actualizar el lector de la consola del pedido.");
        }

        if (pedidoActualizar.getCantidadProcesadores() != pedidoDb.getCantidadProcesadores()) {
            throw new Exception("No puede actualizar la cantidad de procesadores del pedido.");
        }

        if (pedidoActualizar.getCapacidadAlmacenamiento() != pedidoDb.getCapacidadAlmacenamiento()) {
            throw new Exception("No puede actualizar la capacidad de almacenamiento del pedido.");
        }

    }
}
