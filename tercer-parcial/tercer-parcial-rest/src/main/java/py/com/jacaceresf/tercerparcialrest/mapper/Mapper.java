package py.com.jacaceresf.tercerparcialrest.mapper;

import org.springframework.jdbc.core.RowMapper;
import py.com.jacaceresf.tercerparcialrest.beans.Pedido;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Created by jcaceresf on 11/13/20
 */
public class Mapper {

    public static class PedidoMapper implements RowMapper<Pedido> {

        @Override
        public Pedido mapRow(ResultSet rs, int i) throws SQLException {
            Pedido pedido = new Pedido();
            pedido.setIdCliente(rs.getInt("cliente_id"));
            pedido.setCantidad(rs.getInt("cantidad"));

            pedido.setCapacidadAlmacenamiento(rs.getInt("cap_almacenamiento"));
            pedido.setCantidadProcesadores(rs.getInt("can_procesadores"));

            pedido.setNombreConsola(rs.getString("nombre_consola"));

            pedido.setLector(rs.getString("lector_disco").charAt(0));
            pedido.setEstado(rs.getString("estado").charAt(0));

            pedido.setFechaEstimadaEntrega(rs.getObject("fecha_entrega", LocalDate.class));
            pedido.setFechaPago(rs.getObject("fecha_pago", LocalDate.class));
            pedido.setFechaSolicitud(rs.getObject("fecha_solicitud", LocalDate.class));

            return pedido;
        }
    }
}
