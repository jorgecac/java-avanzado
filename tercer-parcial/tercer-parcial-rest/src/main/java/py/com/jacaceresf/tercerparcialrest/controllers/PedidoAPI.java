package py.com.jacaceresf.tercerparcialrest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import py.com.jacaceresf.tercerparcialrest.beans.Pedido;
import py.com.jacaceresf.tercerparcialrest.services.PedidoService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by jcaceresf on 11/13/20
 */
@RestController
@RequestMapping(value = "/pedidos-api")
public class PedidoAPI {

    @Autowired
    private PedidoService pedidoService;

    @PostMapping(value = "/guardar", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Pedido> guardarPedido(@RequestBody Pedido pedido) throws Exception {
        return ResponseEntity.ok(pedidoService.guardarPedido(pedido));
    }

    @GetMapping(value = "/pedidos-pendientes", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Pedido>> obtenerPedidosPendientes() throws Exception {
        return ResponseEntity.ok(pedidoService.obtenerPendientes());
    }

    @GetMapping(value = "/pedidos/{idCliente}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Pedido>> obtenerPedidosCliente(@PathVariable(value = "idCliente") int idCliente) throws Exception {
        return ResponseEntity.ok(pedidoService.obtenerPedidosCliente(idCliente));
    }

    @DeleteMapping(value = "/eliminar-pedido")
    public ResponseEntity eliminarPedido(@RequestParam(value = "idCliente") int idCliente,
                                         @RequestParam(value = "fechaSolicitud") String fechaSolicitud)
            throws Exception {

        pedidoService.eliminarPedidoCliente(
                idCliente,
                LocalDate.parse(fechaSolicitud, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        );

        return ResponseEntity.noContent().build();
    }

    @PatchMapping(value = "/actualizar-pedido", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity actualizarPedido(@RequestBody Pedido pedido) throws Exception {

        pedidoService.actualizarPedido(pedido);

        return ResponseEntity.noContent().build();
    }


}
