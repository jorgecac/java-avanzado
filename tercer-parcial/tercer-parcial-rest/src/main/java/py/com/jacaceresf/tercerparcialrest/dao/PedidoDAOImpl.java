package py.com.jacaceresf.tercerparcialrest.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.support.DaoSupport;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;
import py.com.jacaceresf.tercerparcialrest.beans.Pedido;
import py.com.jacaceresf.tercerparcialrest.mapper.Mapper;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcaceresf on 11/13/20
 */
@Repository
public class PedidoDAOImpl extends NamedParameterJdbcDaoSupport implements PedidoDAO {

    private static final Logger logger = LoggerFactory.getLogger(PedidoDAOImpl.class);

    private static final String TABLE_NAME = "public.pedidos";

    private static final String BASE_SQL = String.format("SELECT * FROM %s", TABLE_NAME);

    private static final String GET_PENDIENTES_SQL = String.format("%s WHERE estado = 'S'", BASE_SQL);

    private static final String GET_PEDIDOS_CLIENTE_SQL = String.format("%s WHERE cliente_id = :cliente_id AND estado = 'E'", BASE_SQL);

    private static final String INSERT_SQL = String.format("INSERT INTO %s (fecha_solicitud, cliente_id, nombre_consola, cantidad, lector_disco, cap_almacenamiento, can_procesadores, estado, fecha_entrega, fecha_pago)" +
            " VALUES (:fecha_solicitud, :cliente_id, :nombre_consola, :cantidad, :lector_disco, :cap_almacenamiento, :can_procesadores, :estado, :fecha_entrega, :fecha_pago)", TABLE_NAME);

    private static final String DELETE_SQL = String.format("DELETE FROM %s WHERE cliente_id = :cliente_id AND fecha_solicitud = :fecha_solicitud", TABLE_NAME);

    private static final String UPDATE_SQL = String.format("UPDATE %s SET fecha_pago = :fecha_pago, fecha_entrega = :fecha_entrega, estado = :estado WHERE cliente_id = :cliente_id AND fecha_solicitud = :fecha_solicitud", TABLE_NAME);

    private static final String VERIFICAR_EXISTENCIA_SQL = String.format("SELECT count(1) FROM %s WHERE cliente_id = :cliente_id AND nombre_consola = :nombre_consola AND fecha_solicitud = :fecha_solicitud", TABLE_NAME);

    private static final String GET_PEDIDO_SQL = String.format("%s WHERE cliente_id = :cliente_id AND nombre_consola = :nombre_consola AND fecha_solicitud = :fecha_solicitud", BASE_SQL);

    @Autowired
    public PedidoDAOImpl(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @Override
    public Pedido guardarPedido(Pedido pedido) throws Exception {

        try {

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("fecha_solicitud", pedido.getFechaSolicitud());
            params.addValue("cliente_id", pedido.getIdCliente());
            params.addValue("nombre_consola", pedido.getNombreConsola());

            params.addValue("cantidad", pedido.getCantidad());
            params.addValue("lector_disco", pedido.getLector());
            params.addValue("cap_almacenamiento", pedido.getCapacidadAlmacenamiento());
            params.addValue("can_procesadores", pedido.getCantidadProcesadores());

            params.addValue("estado", pedido.getEstado());
            params.addValue("fecha_entrega", null);
            params.addValue("fecha_pago", null);

            getNamedParameterJdbcTemplate().update(INSERT_SQL, params);

        } catch (DuplicateKeyException e) {
            logger.error("DuplicateKeyException al intentar guardar el pedido => ", e);
            throw new Exception("Ya existe un pedido con la fecha de solicitud y el cliente especificados.");
        } catch (DataAccessException e) {
            logger.error("Ocurrio un error al guardar el pedido -> ", e);
            throw new Exception("No se puede insertar el pedido.");
        }
        return pedido;
    }

    @Override
    public List<Pedido> obtenerPendientes() throws Exception {
        List<Pedido> result = new ArrayList<>();
        try {

            result = getNamedParameterJdbcTemplate().query(GET_PENDIENTES_SQL, new Mapper.PedidoMapper());

        } catch (DataAccessException e) {
            logger.error("Ocurrio un error al obtener los pedidos -> ", e);
            throw new Exception("Ocurrio un error de BBDD al obtener los pedidos pendientes.");
        } catch (Exception e) {
            logger.error("Ocurrio un error inesperado al obtener los pedidos -> ", e);
            throw new Exception("Ocurrio un error de inesperado al obtener los pedidos pendientes.");
        }
        return result;
    }

    @Override
    public List<Pedido> obtenerPedidosClient(int idCliente) throws Exception {
        try {

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("cliente_id", idCliente);

            return getNamedParameterJdbcTemplate().query(GET_PEDIDOS_CLIENTE_SQL, params, new Mapper.PedidoMapper());
        } catch (DataAccessException e) {
            logger.error("Ocurrio un error al obtener el pedido de un cliente -> ", e);
            throw new Exception("Ocurrio un error de BBDD al obtener los pedidos entregados del cliente " + idCliente);
        }
    }

    @Override
    public void eliminarPedidoClient(int idCliente, LocalDate fechaPedido) throws Exception {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("fecha_solicitud", fechaPedido);
            params.addValue("cliente_id", idCliente);

            getNamedParameterJdbcTemplate().update(DELETE_SQL, params);

        } catch (DataAccessException e) {
            logger.error("Ocurrio un error al eliminar el pedido -> ", e);
            throw new Exception("Ocurrio un error al eliminar el pedido.");
        }
    }

    @Override
    public void actualizarPedido(Pedido pedido) throws Exception {
        try {

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("fecha_pago", pedido.getFechaPago());
            params.addValue("fecha_entrega", pedido.getFechaEstimadaEntrega());
            params.addValue("estado", pedido.getEstado());
            params.addValue("cliente_id", pedido.getIdCliente());
            params.addValue("fecha_solicitud", pedido.getFechaSolicitud());

            getNamedParameterJdbcTemplate().update(UPDATE_SQL, params);

        } catch (DataAccessException e) {
            logger.error("Ocurrio un error al actualizar el pedido -> ", e);
            throw new Exception("No se puede actualizar el pedido.");
        }
    }

    @Override
    public Pedido obtenerPedido(int idCliente, LocalDate fechaPedido, String nombreConsola) throws Exception {
        try {

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("cliente_id", idCliente);
            params.addValue("fecha_solicitud", fechaPedido);
            params.addValue("nombre_consola", nombreConsola);

            return getNamedParameterJdbcTemplate().queryForObject(GET_PEDIDO_SQL, params, new Mapper.PedidoMapper());

        } catch (EmptyResultDataAccessException e) {
            logger.error("EmptyResultDataAccessException al obtener el pedido, no existe");
            throw new Exception("El pedido que intenta actualizar no existe", e);
        } catch (DataAccessException e) {
            logger.error("Ocurrio un error al obtener el pedido -> ", e);
            throw new Exception("No se puede actualizar el pedido.");
        }
    }

    @Override
    public boolean verificarExistencia(int idCliente, LocalDate fechaSolicitud, String nombreConsola) throws Exception {
        try {

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("cliente_id", idCliente);
            params.addValue("fecha_solicitud", fechaSolicitud);
            params.addValue("nombre_consola", nombreConsola);

            int result = getNamedParameterJdbcTemplate().queryForObject(VERIFICAR_EXISTENCIA_SQL, params, int.class);

            return result == 1;

        } catch (DataAccessException e) {
            logger.error("Ocurrio un error al verificar el pedido -> ", e);
            throw new Exception("No se puede verificar la existencia del pedido.");
        }
    }


}
