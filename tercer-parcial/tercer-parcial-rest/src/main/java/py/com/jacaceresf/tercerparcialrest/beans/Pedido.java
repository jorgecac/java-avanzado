package py.com.jacaceresf.tercerparcialrest.beans;

import java.time.LocalDate;

/**
 * Created by jcaceresf on 11/13/20
 */
public class Pedido {

    /**
     * El pedido de una consola está compuesto por el id del cliente, la fecha de solicitud, el nombre de la consola,
     * la cantidad, un indicador para saber si la consola tiene lector de disco (S/N), capacidad de almacenamiento en GB,
     * cantidad de procesadores, el estado del pedido (S=solicitado, E=entregado), la fecha estimada de entrega y la fecha de pago.
     */


    private int idCliente;
    private LocalDate fechaSolicitud;
    private String nombreConsola;
    private int cantidad;
    private char lector;
    private int capacidadAlmacenamiento;
    private int cantidadProcesadores;
    private char estado;
    private LocalDate fechaEstimadaEntrega;
    private LocalDate fechaPago;

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public LocalDate getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(LocalDate fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getNombreConsola() {
        return nombreConsola;
    }

    public void setNombreConsola(String nombreConsola) {
        this.nombreConsola = nombreConsola;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public char getLector() {
        return lector;
    }

    public void setLector(char lector) {
        this.lector = lector;
    }

    public int getCapacidadAlmacenamiento() {
        return capacidadAlmacenamiento;
    }

    public void setCapacidadAlmacenamiento(int capacidadAlmacenamiento) {
        this.capacidadAlmacenamiento = capacidadAlmacenamiento;
    }

    public int getCantidadProcesadores() {
        return cantidadProcesadores;
    }

    public void setCantidadProcesadores(int cantidadProcesadores) {
        this.cantidadProcesadores = cantidadProcesadores;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }

    public LocalDate getFechaEstimadaEntrega() {
        return fechaEstimadaEntrega;
    }

    public void setFechaEstimadaEntrega(LocalDate fechaEstimadaEntrega) {
        this.fechaEstimadaEntrega = fechaEstimadaEntrega;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "idCliente=" + idCliente +
                ", fechaSolicitud=" + fechaSolicitud +
                ", nombreConsola='" + nombreConsola + '\'' +
                ", cantidad=" + cantidad +
                ", lector=" + lector +
                ", capacidadAlmacenamiento=" + capacidadAlmacenamiento +
                ", cantidadProcesadores=" + cantidadProcesadores +
                ", estado=" + estado +
                ", fechaEstimadaEntrega=" + fechaEstimadaEntrega +
                ", fechaPago=" + fechaPago +
                '}';
    }
}
