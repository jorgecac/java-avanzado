package py.com.jacaceresf.tercerparcialrest.dao;

import com.sun.org.apache.xml.internal.resolver.readers.ExtendedXMLCatalogReader;
import py.com.jacaceresf.tercerparcialrest.beans.Pedido;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by jcaceresf on 11/13/20
 */
public interface PedidoDAO {

    Pedido guardarPedido(Pedido pedido) throws Exception;

    List<Pedido> obtenerPendientes() throws Exception;

    List<Pedido> obtenerPedidosClient(int idCliente) throws Exception;

    void eliminarPedidoClient(int idCliente, LocalDate fechaPedido) throws Exception;

    void actualizarPedido(Pedido pedido) throws Exception;

    Pedido obtenerPedido(int idCliente, LocalDate fechaPedido, String nombreConsola) throws Exception;

    boolean verificarExistencia(int idCliente, LocalDate fechaSolicitud, String nombreConsola) throws Exception;

}
