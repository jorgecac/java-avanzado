package py.com.jacaceresf.tercerparcialrest.handler;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import py.com.jacaceresf.tercerparcialrest.beans.Error;

import java.time.LocalDateTime;

/**
 * Created by jcaceresf on 11/13/20
 */
@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Error> handleEcommerceException(Exception exception) {

        Error error = new Error();
        error.setMessage(exception.getMessage());
        error.setTstamp(LocalDateTime.now());

        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        if (exception.getCause() != null && exception.getCause().getClass().equals(EmptyResultDataAccessException.class)) {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(error, httpStatus);
    }
}
