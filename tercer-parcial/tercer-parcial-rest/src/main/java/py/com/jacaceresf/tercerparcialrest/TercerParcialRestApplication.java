package py.com.jacaceresf.tercerparcialrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TercerParcialRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TercerParcialRestApplication.class, args);
	}

}
