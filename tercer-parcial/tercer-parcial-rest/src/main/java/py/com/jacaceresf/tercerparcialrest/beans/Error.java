package py.com.jacaceresf.tercerparcialrest.beans;

import java.time.LocalDateTime;

/**
 * Created by jcaceresf on 11/13/20
 */
public class Error {

    private String message;
    private LocalDateTime tstamp;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTstamp() {
        return tstamp;
    }

    public void setTstamp(LocalDateTime tstamp) {
        this.tstamp = tstamp;
    }
}
