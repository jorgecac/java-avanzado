package py.com.jacaceresf.tercerparcialrest.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import py.com.jacaceresf.tercerparcialrest.beans.Pedido;
import py.com.jacaceresf.tercerparcialrest.services.PedidoService;
import py.com.jacaceresf.tercerparcialrest.services.PedidoServiceImpl;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;

/**
 * Created by jcaceresf on 11/14/20
 */
@SpringBootTest
@EnableConfigurationProperties
public class PedidosTest {

    /**
     * (15 ptos) Crear pruebas unitarias para cada uno de los métodos asociados con los servicios de alta,
     * eliminación, actualización y consulta. Las pruebas unitarias deben tener los casos de éxitos
     * como así también aquellos casos en donde se debe activar algún control que este especificado en cada uno de los servicios REST.
     */

    private PedidoService pedidoService;

    @Before
    public void init() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUrl("jdbc:postgresql://localhost:5432/gamersa");
        ds.setUsername("postgres");
        ds.setPassword("postgres");
        pedidoService = new PedidoServiceImpl(ds);
    }

    @Test
    public void guardarPedidoTest() throws Exception {
        Pedido pedido = new Pedido();
        pedido.setNombreConsola("Consola Test");
        pedido.setEstado('S');
        pedido.setCantidadProcesadores(1);
        pedido.setCantidad(2);
        pedido.setCapacidadAlmacenamiento(256);
        pedido.setLector('N');
        pedido.setFechaSolicitud(LocalDate.now());
        pedido.setIdCliente(2);
        Pedido pedido1 = pedidoService.guardarPedido(pedido);
        Assert.assertEquals(pedido, pedido1);
    }

    @Test(expected = Exception.class)
    public void guardarPedidoErrorTest() throws Exception {
        try {
            Pedido pedido = new Pedido();
            pedido.setNombreConsola("Consola 1");
            pedido.setEstado('E'); //ESTADO INVALIDO
            pedido.setCantidadProcesadores(1);
            pedido.setCantidad(2);
            pedido.setCapacidadAlmacenamiento(256);
            pedido.setLector('N');
            pedido.setFechaSolicitud(LocalDate.now());
            pedido.setIdCliente(1);
            pedidoService.guardarPedido(pedido);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test
    public void obtenerPedidosPendientesTest() throws Exception {
        List<Pedido> pedidos = pedidoService.obtenerPendientes();
        System.out.println("Tengo " + pedidos.size() + " pedidos pendientes");
    }

    @Test
    public void obtenerPedidosClienteTest() throws Exception {
        List<Pedido> pedidos = pedidoService.obtenerPedidosCliente(1);
        Assert.assertTrue(pedidos != null && pedidos.size() > 0);
    }

    @Test
    public void actualizarPedidoTest() throws Exception {
        List<Pedido> pedidos = pedidoService.obtenerPendientes();
        Pedido pedido = pedidos.get(0);
        System.out.println("Voy a actualizar el pedido => " + pedido);
        pedido.setFechaPago(LocalDate.now());
        pedido.setFechaPago(LocalDate.now());
        pedido.setEstado('E');
        pedidoService.actualizarPedido(pedido);
    }

    @Test(expected = Exception.class)
    public void actualizarPedidoErrorTest() throws Exception {
        try {
            List<Pedido> pedidos = pedidoService.obtenerPendientes();
            Pedido pedido = pedidos.get(0);
            System.out.println("Voy a actualizar el pedido => " + pedido);
            pedido.setCapacidadAlmacenamiento(new Random().nextInt());
            pedido.setCantidad(new Random().nextInt());
            pedido.setFechaPago(LocalDate.now());
            pedido.setFechaPago(LocalDate.now());
            pedido.setEstado('E');
            pedidoService.actualizarPedido(pedido);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test
    public void eliminarPedidoTest() throws Exception {
        pedidoService.eliminarPedidoCliente(1, LocalDate.now());
    }

    @Test(expected = Exception.class)
    public void eliminarPedidoErrorTest() throws Exception {
        try {
            pedidoService.eliminarPedidoCliente(1, null);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}
